# Sunburst #

Sunburst is a Windows desktop application for visualizing numerical data that have hierarchical structure at a chart called sunburst. This type of chart is especially useful for summarizing the data and presenting the relations between hierarchy levels. 

This is an academic project implemented for classes of Information Visualization subject (edition 2014/2015) carried at Gdansk University of Technology, Faculty of Electronics, Telecommunications and Informatics. It is **not maintained**.

Prebuilt binaries are available in *Downloads* section.

**Useful tip:** some interesting options are available after RMB click on chart parts, eg. expanding and reordering child nodes.

*Note*: Sunburst app can read only data in custom XML format. XML Schema file that is used to validate input data is located in:

```
#!c#
SunburstWPF/Resources/SunburstSchema.xsd

```
Example XML files with input data (in Polish) are located in:

```
#!c#
SunburstWPF/ExampleData
```

## How do I get set up? ##
In case anyone wished to continue this work, here's what I used to build the project: .NET Framework 4.0, C#, WPF, [Xceed WPF Toolkit](http://wpftoolkit.codeplex.com/), Visual Studio 2010.

## Screenshots ##
Alfabet.xml:
![Sunburst-alphabet.png](https://bitbucket.org/repo/5qx9x9b/images/2088652086-Sunburst-alphabet.png)

### License ###
MIT/X11
Copyright (c) 2015 Monika Siedlecka