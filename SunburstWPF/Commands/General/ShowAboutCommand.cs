﻿using System;
using System.Windows;
using System.Windows.Input;
using SunburstWPF.Languages;

namespace SunburstWPF.Commands.General
{
    public class ShowAboutCommand : HotKeyCommand, ICommand
    {
        public ShowAboutCommand()
        {
            Text = Lang.About;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            MessageBox.Show(Lang.ProjectIV1415 + Environment.NewLine + Lang.AuthorMe,
                Lang.About, MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }
}