﻿using System;
using System.Windows;
using System.Windows.Input;
using SunburstWPF.Languages;
using SunburstWPF.ViewModels;
using SunburstWPF.Views;

namespace SunburstWPF.Commands.General
{
    public class ChangeLanguageCommand : HotKeyCommand, ICommand
    {
        public ChangeLanguageCommand()
        {
            Text = Lang.SetLanguage;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            LanguageViewModel languageViewModel = new LanguageViewModel();
            LanguageChangerView languageView = new LanguageChangerView
            {
                DataContext = languageViewModel,
                Owner = parameter as Window
            };
            languageView.Closing += languageViewModel.OnViewClosing;
            languageView.Show();
        }
    }
}