﻿using System;
using System.Windows;
using System.Windows.Input;
using SunburstWPF.ChartModel;
using SunburstWPF.ViewModels;
using SunburstWPF.Views;

namespace SunburstWPF.Commands.PresentationChange
{
    public class ChangeColorCommand : HotKeyCommand, ICommand
    {
        private ARGBColor colorToChange;

        public ChangeColorCommand(string text)
        {
            Text = text;
        }

        public ChangeColorCommand(ARGBColor color, string text) : this(text)
        {
            colorToChange = color;
        }

        public ARGBColor ColorToChange
        {
            private get { return colorToChange; }
            set { colorToChange = value; }
        }

        public bool CanExecute(object parameter)
        {
            return ColorToChange != null;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            ColorViewModel colorViewModel = new ColorViewModel(colorToChange);
            ColorPickerView colorPicker = new ColorPickerView
            {
                DataContext = colorViewModel,
                Owner = parameter as Window
            };
            colorPicker.Closing += colorViewModel.OnViewClosing;
            colorPicker.Show();
        }
    }
}