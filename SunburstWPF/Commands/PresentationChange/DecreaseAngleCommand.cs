﻿using System.Windows.Input;
using SunburstWPF.Helpers;
using SunburstWPF.Languages;

namespace SunburstWPF.Commands.PresentationChange
{
    public class DecreaseAngleCommand : ChangeValueCommand<double>
    {
        public DecreaseAngleCommand()
        {
            Text = Lang.SetRotationAngle;
            Gesture = new KeyGesture(Key.OemComma, ModifierKeys.Shift); // Shift + '<'
        }

        protected override void ChangeValueAction(ref double value)
        {
            if (value >= SettingsHelper.GetAngleChangeButtonStep())
                value -= SettingsHelper.GetAngleChangeButtonStep();
        }
    }
}