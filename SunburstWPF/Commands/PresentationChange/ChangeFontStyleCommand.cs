﻿using System;
using System.Windows;
using System.Windows.Input;
using SunburstWPF.ChartModel;
using SunburstWPF.ViewModels;
using SunburstWPF.Views;

namespace SunburstWPF.Commands.PresentationChange
{
    public class ChangeFontStyleCommand : HotKeyCommand, ICommand
    {
        private FontDefinitionType fontStyle;

        public ChangeFontStyleCommand(string text)
        {
            Text = text;
        }

        public ChangeFontStyleCommand(FontDefinitionType style, string text)
            : this(text)
        {
            fontStyle = style;
        }

        public FontDefinitionType FontStyleToChange
        {
            get { return fontStyle; }
            set { fontStyle = value; }
        }

        public bool CanExecute(object parameter)
        {
            return FontStyleToChange != null;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            FontPropertyViewModel fontViewModel = new FontPropertyViewModel(fontStyle);
            FontStyleChangerView fontPropertyChanger = new FontStyleChangerView
            {
                DataContext = fontViewModel,
                Owner = parameter as Window
            };
            fontPropertyChanger.Closing += fontViewModel.OnViewClosing;
            fontPropertyChanger.Show();
        }
    }
}