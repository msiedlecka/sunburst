﻿using System.Windows.Input;
using SunburstWPF.Helpers;
using SunburstWPF.Languages;

namespace SunburstWPF.Commands.PresentationChange
{
    public class IncrementZoomCommand : ChangeValueCommand<int>
    {
        public IncrementZoomCommand()
        {
            Text = Lang.IncreaseZoom;
            Gesture = new KeyGesture(Key.OemPlus, ModifierKeys.Control); // Add == plus on the numpad
        }

        protected override void ChangeValueAction(ref int value)
        {
            if (value < 1000 - SettingsHelper.GetZoomChangeButtonStep() + 1) // maximum value value is 1000
                value += SettingsHelper.GetZoomChangeButtonStep();
        }
    }
}