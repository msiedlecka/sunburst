﻿using System;
using System.Windows;
using System.Windows.Input;
using SunburstWPF.ChartModel;
using SunburstWPF.ViewModels;
using SunburstWPF.Views;

namespace SunburstWPF.Commands.PresentationChange
{
    public class ChangeLineStyleCommand : HotKeyCommand, ICommand
    {
        private LineStyleType lineStyle;

        public ChangeLineStyleCommand(string text)
        {
            Text = text;
        }

        public ChangeLineStyleCommand(LineStyleType style, string text)
            : this(text)
        {
            lineStyle = style;
        }

        public LineStyleType LineStyleToChange
        {
            get { return lineStyle; }
            set { lineStyle = value; }
        }

        public bool CanExecute(object parameter)
        {
            return LineStyleToChange != null;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            LinePropertyViewModel lineViewModel = new LinePropertyViewModel(lineStyle);
            LinePropertyChangerView linePropertyChanger = new LinePropertyChangerView
            {
                DataContext = lineViewModel,
                Owner = parameter as Window
            };
            linePropertyChanger.Closing += lineViewModel.OnViewClosing;
            linePropertyChanger.Show();
        }
    }
}