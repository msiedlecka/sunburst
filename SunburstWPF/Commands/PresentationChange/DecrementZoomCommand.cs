﻿using System.Windows.Input;
using SunburstWPF.Helpers;
using SunburstWPF.Languages;

namespace SunburstWPF.Commands.PresentationChange
{
    public class DecrementZoomCommand : ChangeValueCommand<int>
    {
        public DecrementZoomCommand()
        {
            Text = Lang.DecreaseZoom;
            Gesture = new KeyGesture(Key.OemMinus, ModifierKeys.Control); // Subtract == minus on the numpad
        }

        protected override void ChangeValueAction(ref int value)
        {
            if (value > SettingsHelper.GetZoomChangeButtonStep())
                value -= SettingsHelper.GetZoomChangeButtonStep(); // minimal value value is 1
        }
    }
}