﻿using System.Windows.Input;
using SunburstWPF.Helpers;
using SunburstWPF.Languages;

namespace SunburstWPF.Commands.PresentationChange
{
    public class IncreaseAngleCommand : ChangeValueCommand<double>
    {
        public IncreaseAngleCommand()
        {
            Text = Lang.SetRotationAngle;
            Gesture = new KeyGesture(Key.OemPeriod, ModifierKeys.Shift); // Shift + '>'
        }

        protected override void ChangeValueAction(ref double value)
        {
            if (value < 360 - SettingsHelper.GetAngleChangeButtonStep() - 0.001)
                value += SettingsHelper.GetAngleChangeButtonStep();
        }
    }
}