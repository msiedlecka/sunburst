﻿using System;
using System.Windows.Input;
using SunburstWPF.ChartModel;
using SunburstWPF.Languages;

namespace SunburstWPF.Commands.PresentationChange
{
    public class LevelUpCommand : HotKeyCommand, ICommand
    {
        private Chart chart;

        public LevelUpCommand()
        {
            Text = Lang.GoLevelUp;
            Gesture = new KeyGesture(Key.Space, ModifierKeys.Control);
        }

        public LevelUpCommand(Chart chart) : this()
        {
            this.chart = chart;
        }

        public Chart ChartToModify
        {
            private get { return chart; }
            set { chart = value; }
        }

        public bool CanExecute(object parameter)
        {
            return ChartToModify != null && ChartToModify.GeneralChartDesign.CurrentRootId != 0;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            int currentRootId = ChartToModify.GeneralChartDesign.CurrentRootId;
            NodeType currentRoot = ChartToModify.GetNodeById(currentRootId);
            int newRootId = currentRoot.Parent != null ? currentRoot.Parent.Id : 0;
            ChartToModify.GeneralChartDesign.CurrentRootId = newRootId;
        }
    }
}