﻿using System;
using System.Windows.Input;

namespace SunburstWPF.Commands.PresentationChange
{
    public abstract class ChangeValueCommand<T> : HotKeyCommand, ICommand
    {
        public Action<T> AfterExecuted;
        public Func<T> BeforeExecuted;

        public bool CanExecute(object parameter)
        {
            return BeforeExecuted != null;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            T value;
            if (BeforeExecuted != null)
            {
                value = BeforeExecuted.Invoke();
                ChangeValueAction(ref value);
                if (AfterExecuted != null)
                    AfterExecuted.Invoke(value);
            }
        }

        protected abstract void ChangeValueAction(ref T value);
    }
}