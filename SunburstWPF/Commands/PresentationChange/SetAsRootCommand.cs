﻿using System;
using System.Windows.Input;
using SunburstWPF.ChartModel;

namespace SunburstWPF.Commands.PresentationChange
{
    public class SetAsRootCommand : HotKeyCommand, ICommand
    {
        private GeneralChartDesign generalDesign;
        private int nodeId;

        public SetAsRootCommand(string text, int nodeId, bool useGesture = false)
        {
            Text = text;
            this.nodeId = nodeId;
            if (useGesture)
                Gesture = new KeyGesture(Key.Enter, ModifierKeys.Control);
        }

        public SetAsRootCommand(string text, int nodeId, GeneralChartDesign generalDesign, bool useGesture = false)
            : this(text, nodeId, useGesture)
        {
            this.generalDesign = generalDesign;
        }

        public GeneralChartDesign GeneralDesign
        {
            private get { return generalDesign; }
            set { generalDesign = value; }
        }

        public bool CanExecute(object parameter)
        {
            return GeneralDesign != null && GeneralDesign.CurrentRootId != nodeId;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            GeneralDesign.CurrentRootId = nodeId;
        }
    }
}