﻿using System;
using System.Windows.Input;
using SunburstWPF.Languages;

namespace SunburstWPF.Commands.FileMenu
{
    public class CloseCommand : HotKeyCommand, ICommand
    {
        public Action AfterExecuted;

        public Action BeforeExecute;

        public CloseCommand()
        {
            Text = Lang.Close;
            Gesture = new KeyGesture(Key.F4, ModifierKeys.Alt);
        }

        public bool Cancelled { get; set; }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            if (BeforeExecute != null)
            {
                BeforeExecute.Invoke(); // Cancelled set in there
                if (!Cancelled)
                    if (AfterExecuted != null)
                        AfterExecuted.Invoke();
            }
        }
    }
}