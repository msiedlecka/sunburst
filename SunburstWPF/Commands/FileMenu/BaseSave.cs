﻿using System;
using System.IO;
using System.Windows.Input;
using SunburstWPF.ChartModel;
using SunburstWPF.FileHandlers.DataSource;

namespace SunburstWPF.Commands.FileMenu
{
    public abstract class BaseSave : HotKeyCommand, ICommand
    {
        public Func<Chart> BeforeExecuted;
        protected Chart chart;

        public bool CanExecute(object parameter)
        {
            return BeforeExecuted != null;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            if (BeforeExecuted != null)
            {
                chart = BeforeExecuted.Invoke();
                SaveAction();
            }
        }

        protected void Save(Chart chart, string filePath)
        {
            IChartFileParser parser = FileParserFactory.CreateParser(Path.GetExtension(filePath));
            parser.Save(chart, filePath);
        }

        protected abstract void SaveAction();
    }
}