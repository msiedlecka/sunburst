﻿using System;
using System.Windows.Input;
using Microsoft.Win32;
using SunburstWPF.Helpers;
using SunburstWPF.Languages;

namespace SunburstWPF.Commands.FileMenu
{
    public class SaveToFileCommand : BaseSave
    {
        public SaveToFileCommand()
        {
            Text = Lang.SaveAs;
            Gesture = new KeyGesture(Key.None, ModifierKeys.None);
        }

        protected override void SaveAction()
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.AddExtension = true;
            saveFileDialog.DereferenceLinks = true;
            saveFileDialog.Title = Lang.Save;
            saveFileDialog.Filter = FileDialogHelper.CreateExtensionsFilter(SettingsHelper.GetSupportedExtensions());
            saveFileDialog.DefaultExt = FileDialogHelper.GetDefaultExtension(SettingsHelper.GetSupportedExtensions());
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyComputer);
            saveFileDialog.FileName = SettingsHelper.GetDefaultFileName();

            bool? userDidChooseFile = saveFileDialog.ShowDialog();
            if (userDidChooseFile == true)
            {
                string filePath = saveFileDialog.FileName;
                Save(chart, filePath);
            }
        }
    }
}