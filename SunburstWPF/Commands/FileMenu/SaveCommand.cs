﻿using System.Windows.Input;
using SunburstWPF.Languages;

namespace SunburstWPF.Commands.FileMenu
{
    public class SaveCommand : BaseSave
    {
        public SaveCommand()
        {
            Text = Lang.Save;
            Gesture = new KeyGesture(Key.S, ModifierKeys.Control);
        }

        protected override void SaveAction()
        {
            string filePath = chart.PathToFile;
            Save(chart, filePath);
        }
    }
}