﻿using System;
using System.IO;
using System.Windows.Controls;
using System.Windows.Input;
using Microsoft.Win32;
using SunburstWPF.FileHandlers.Image;
using SunburstWPF.Helpers;
using SunburstWPF.Languages;

namespace SunburstWPF.Commands.FileMenu
{
    public class SaveAsImageCommand : HotKeyCommand, ICommand
    {
        private ScrollViewer chartViewer;

        public SaveAsImageCommand(ScrollViewer chartViewer)
        {
            Text = Lang.SaveAsImage;
            this.chartViewer = chartViewer;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.AddExtension = true;
            saveFileDialog.DereferenceLinks = true;
            saveFileDialog.Title = Lang.SaveAsImage;
            saveFileDialog.Filter = FileDialogHelper.CreateExtensionsFilter(SettingsHelper.GetSupportedImageExtensions());
            saveFileDialog.DefaultExt =
                FileDialogHelper.GetDefaultExtension(SettingsHelper.GetSupportedImageExtensions());
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyComputer);
            saveFileDialog.FileName = SettingsHelper.GetDefaultFileName();

            bool? userDidChooseFile = saveFileDialog.ShowDialog();
            if (userDidChooseFile == true)
            {
                Grid chartGrid = chartViewer.FindName(SettingsHelper.GetChartGridName()) as Grid;
                string filePath = saveFileDialog.FileName;
                ImageSaver saver = ImageSaverFactory.CreateSaver(Path.GetExtension(filePath));
                saver.Save(chartGrid, filePath);
                ImageSaver.MeasureArrangeVisual(chartViewer);
            }
        }
    }
}