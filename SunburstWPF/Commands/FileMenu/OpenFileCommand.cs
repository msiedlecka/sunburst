﻿using System;
using System.IO;
using System.Windows.Input;
using Microsoft.Win32;
using SunburstWPF.ChartModel;
using SunburstWPF.FileHandlers.DataSource;
using SunburstWPF.Helpers;
using SunburstWPF.Languages;

namespace SunburstWPF.Commands.FileMenu
{
    public class OpenFileCommand : HotKeyCommand, ICommand
    {
        private Chart chart;

        public OpenFileCommand()
        {
            Text = Lang.Open;
            Gesture = new KeyGesture(Key.O, ModifierKeys.Control);
        }

        public Action<Chart> AfterExecuted { get; set; }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.CheckFileExists = true;
            openFileDialog.CheckPathExists = true;
            openFileDialog.Multiselect = false;
            openFileDialog.DereferenceLinks = true;
            openFileDialog.Title = Lang.Open;
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyComputer);
            openFileDialog.Filter = FileDialogHelper.CreateExtensionsFilter(SettingsHelper.GetSupportedExtensions());

            bool? userDidChooseFile = openFileDialog.ShowDialog();
            if (userDidChooseFile == true)
            {
                string filePath = openFileDialog.FileName;
                IChartFileParser parser = FileParserFactory.CreateParser(Path.GetExtension(filePath));
                chart = parser.Parse(filePath);

                if (chart != null) // null means that something in a file was wrong
                {
                    chart.PathToFile = filePath;
                    InitializeChart();
                }

                if (AfterExecuted != null)
                    AfterExecuted.Invoke(chart);
            }
        }

        private void InitializeChart()
        {
            chart.FillMissingStyles(SettingsHelper.GetTitleFont(), SettingsHelper.GetDescriptionFont(),
                SettingsHelper.GetLabelFont(),
                SettingsHelper.GetDefaultBackgroundColor(), SettingsHelper.GetLinesStyle(),
                SettingsHelper.GetLinesStyle(), SettingsHelper.GetPalette(),
                SettingsHelper.GetZoom(), SettingsHelper.GetRotationAngle());
            chart.CheckAllAbbreviations();
            chart.FindDeepestLevel();
            chart.SetParents();
            chart.PropagateUnits();
        }
    }
}