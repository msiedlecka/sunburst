﻿using System.Windows.Input;
using SunburstWPF.Helpers;

namespace SunburstWPF.Commands
{
    public abstract class HotKeyCommand
    {
        public KeyGesture Gesture { get; set; }

        public string GestureText
        {
            get
            {
                return Gesture != null ? Gesture.GetDisplayStringForCulture(SettingsHelper.GetCurrentLanguage()) : "";
            }
        }

        public string Text { get; set; }

        public string ToolTip
        {
            get { return GestureText.Equals("") ? Text : Text + string.Format(" ({0})", GestureText); }
        }
    }
}