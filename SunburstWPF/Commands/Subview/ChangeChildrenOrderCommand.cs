﻿using System;
using System.Windows.Input;
using SunburstWPF.ChartModel;
using SunburstWPF.Languages;
using SunburstWPF.ViewModels;
using SunburstWPF.Views;

namespace SunburstWPF.Commands.Subview
{
    public class ChangeChildrenOrderCommand : HotKeyCommand, ICommand
    {
        private NodeType parent;

        public ChangeChildrenOrderCommand(NodeType parentNode)
        {
            Text = Lang.ChangeChildrenOrder;
            parent = parentNode;
        }

        public bool CanExecute(object parameter)
        {
            return parent != null && parent.ChildNodes != null && parent.ChildNodes.Count > 1;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            ChildrenOrderViewModel orderViewModel = new ChildrenOrderViewModel(parent);
            ChildrenOrderChangerView orderView = new ChildrenOrderChangerView
            {
                DataContext = orderViewModel
            };
            orderView.Closing += orderViewModel.OnViewClosing;
            orderView.Show();
        }
    }
}