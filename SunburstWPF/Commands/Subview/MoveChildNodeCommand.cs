﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using ExtensionMethods;
using SunburstWPF.ChartModel;

namespace SunburstWPF.Commands.Subview
{
    public class MoveChildNodeCommand : HotKeyCommand, ICommand
    {
        private int movedNodeIndex = -1;
        // states if node is moved at position with index = index + 1 [true] or index - 1 [false]
        private bool moveForward = true;
        private ObservableCollection<NodeType> nodes;

        public MoveChildNodeCommand(string text, KeyGesture gesture, ObservableCollection<NodeType> nodes,
            bool moveForward)
        {
            Text = text;
            Gesture = gesture;
            this.nodes = nodes;
            this.moveForward = moveForward;
        }

        public int MovedNodeIndex
        {
            private get { return movedNodeIndex; }
            set { movedNodeIndex = value; }
        }

        public bool CanExecute(object parameter)
        {
            if (nodes != null && MovedNodeIndex >= 0)
            {
                int nodesCount = nodes.Count;
                if ((moveForward && MovedNodeIndex < nodesCount - 1) || (!moveForward && MovedNodeIndex > 0))
                    return true;
            }
            return false;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        // correct conditions (index and moveFormard fields' values) should be provided by CanExecute()
        public void Execute(object parameter)
        {
            int indexToSwapWith = moveForward ? movedNodeIndex++ : movedNodeIndex--;
            nodes.Swap(movedNodeIndex, indexToSwapWith);
        }
    }
}