﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace SunburstWPF.Converters
{
    // basically, Covert() multiplies the value by 100
    public class PercentageToDimesionConverter : IValueConverter
    {
        private static string argumentException =
            "Object of unsupported type [{0}] passed to PercentageToDimesionConverter.{1}()";

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;

            if (value is double)
            {
                double doubleValue = (double) value;
                return doubleValue*100.0;
            }

            throw new ArgumentException(string.Format(argumentException, value.GetType().Name, "Convert"), "value");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;

            if (value is double)
            {
                double doubleValue = (double) value;
                return doubleValue/100.0;
            }

            throw new ArgumentException(string.Format(argumentException, value.GetType().Name, "ConvertBack"), "value");
        }
    }
}