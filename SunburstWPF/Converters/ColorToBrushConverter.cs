﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace SunburstWPF.Converters
{
    public class ColorToBrushConverter : IValueConverter
    {
        private static string argumentException =
            "Object of unsupported type [{0}] passed to ColorToBrushConverter.{1}()";

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;

            if (value is Color)
                return new SolidColorBrush((Color) value);

            throw new ArgumentException(string.Format(argumentException, value.GetType().Name, "Convert"), "value");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;

            if (value is SolidColorBrush)
                return ((SolidColorBrush) value).Color;

            throw new ArgumentException(string.Format(argumentException, value.GetType().Name, "ConvertBack"), "value");
        }
    }
}