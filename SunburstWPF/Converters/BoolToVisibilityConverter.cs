﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace SunburstWPF.Converters
{
    public class BoolToVisibilityConverter : IValueConverter
    {
        private static string argumentException =
            "Object of unsupported type [{0}] passed to BoolToVisibilityConverter.{1}()";

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;

            if (value is bool)
            {
                bool boolValue = (bool) value;
                return boolValue ? Visibility.Visible : Visibility.Collapsed;
            }

            throw new ArgumentException(string.Format(argumentException, value.GetType().Name, "Convert"), "value");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;

            if (value is Visibility)
            {
                Visibility visibilityValue = (Visibility) value;
                return visibilityValue.Equals(Visibility.Visible);
            }

            throw new ArgumentException(string.Format(argumentException, value.GetType().Name, "ConvertBack"), "value");
        }
    }
}