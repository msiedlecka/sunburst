﻿using System.Windows;

namespace SunburstWPF.Views
{
    /// <summary>
    ///     Interaction logic for ColorPickerView.xaml
    /// </summary>
    public partial class ColorPickerView : Window
    {
        public ColorPickerView()
        {
            InitializeComponent();
        }
    }
}