﻿using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SunburstWPF.Drawing
{
    public class ChartPartShape : Shape
    {
        public Point MiddlePoint
        {
            get
            {
                // half way between outer and inner arc
                Point startMiddlePoint = new Point(CenterX, CenterY - InnerRadius - (TotalRadius - InnerRadius)/2);
                // angle in the middle of both arcs
                RotateTransform middleRotate = new RotateTransform(RotationAngle + WedgeAngle/2, CenterX, CenterY);
                return middleRotate.Transform(startMiddlePoint);
            }
        }

        protected override Geometry DefiningGeometry
        {
            get { return CreateRingPart(); }
        }

        private Geometry CreateRingPart()
        {
            if (WedgeAngle < 360)
            {
                // starting from 12 o'clock position
                Point outerArcPrimaryStart = new Point(CenterX, CenterY - TotalRadius);
                Point innerArcPrimaryStart = new Point(CenterX, CenterY - InnerRadius);
                RotateTransform rotation = new RotateTransform(RotationAngle, CenterX, CenterY);

                // creating points that are actually start points for their arcs
                Point outerArcStart = rotation.Transform(outerArcPrimaryStart);
                Point innerArcStart = rotation.Transform(innerArcPrimaryStart);

                rotation.Angle += WedgeAngle; // make it rotate to the end point of the arcs
                Point outerArcEnd = rotation.Transform(outerArcPrimaryStart);
                Point innerArcEnd = rotation.Transform(innerArcPrimaryStart);

                // creating ring part counterclockwise (line - inner arc - line - outer arc)
                LineSegment firstClockwise = new LineSegment(innerArcStart, true);
                ArcSegment innerArc = new ArcSegment(innerArcEnd, new Size(InnerRadius, InnerRadius), 0,
                    WedgeAngle >= 180, SweepDirection.Clockwise, true);
                LineSegment secondClockwise = new LineSegment(outerArcEnd, true);
                ArcSegment outerArc = new ArcSegment(outerArcStart, new Size(TotalRadius, TotalRadius), 0,
                    WedgeAngle >= 180, SweepDirection.Counterclockwise, true);

                PathFigure figure = new PathFigure(outerArcStart,
                    new PathSegment[] {firstClockwise, innerArc, secondClockwise, outerArc}, true);

                return new PathGeometry(new[] {figure}, FillRule.Nonzero, null);
            }
            GeometryGroup group = new GeometryGroup();
            EllipseGeometry innerCircle = new EllipseGeometry(new Point(CenterX, CenterY), InnerRadius, InnerRadius);
            EllipseGeometry outerCircle = new EllipseGeometry(new Point(CenterX, CenterY), TotalRadius, TotalRadius);
            group.FillRule = FillRule.EvenOdd;
            group.Children.Add(innerCircle);
            group.Children.Add(outerCircle);
            return group;
        }

        #region Dependency properties

        public static readonly DependencyProperty TotalRadiusProperty =
            DependencyProperty.Register("TotalRadiusProperty", typeof(double), typeof(ChartPartShape),
                new FrameworkPropertyMetadata(0.0,
                    FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.AffectsMeasure));

        /// <summary>
        ///     The radius of this piece
        /// </summary>
        public double TotalRadius
        {
            get { return (double) GetValue(TotalRadiusProperty); }
            set { SetValue(TotalRadiusProperty, value); }
        }

        public static readonly DependencyProperty InnerRadiusProperty =
            DependencyProperty.Register("InnerRadiusProperty", typeof(double), typeof(ChartPartShape),
                new FrameworkPropertyMetadata(0.0,
                    FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.AffectsMeasure));

        /// <summary>
        ///     The inner radius of this piece
        /// </summary>
        public double InnerRadius
        {
            get { return (double) GetValue(InnerRadiusProperty); }
            set { SetValue(InnerRadiusProperty, value); }
        }

        public static readonly DependencyProperty WedgeAngleProperty =
            DependencyProperty.Register("WedgeAngleProperty", typeof(double), typeof(ChartPartShape),
                new FrameworkPropertyMetadata(0.0,
                    FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.AffectsMeasure));

        /// <summary>
        ///     The wedge angle of this piece in degrees.
        ///     Setting this property changes the value of PercentageProperty (and vice-versa).
        /// </summary>
        public double WedgeAngle
        {
            get { return (double) GetValue(WedgeAngleProperty); }
            private set
            {
                SetValue(WedgeAngleProperty, value);
                SetValue(PercentageProperty, value/360.0);
            }
        }

        public static readonly DependencyProperty RotationAngleProperty =
            DependencyProperty.Register("RotationAngleProperty", typeof(double), typeof(ChartPartShape),
                new FrameworkPropertyMetadata(0.0,
                    FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.AffectsMeasure));

        /// <summary>
        ///     The rotation, in degrees, from the Y axis vector
        /// </summary>
        public double RotationAngle
        {
            get { return (double) GetValue(RotationAngleProperty); }
            set { SetValue(RotationAngleProperty, value); }
        }

        public static readonly DependencyProperty CenterXProperty =
            DependencyProperty.Register("CenterXProperty", typeof(double), typeof(ChartPartShape),
                new FrameworkPropertyMetadata(0.0,
                    FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.AffectsMeasure));

        /// <summary>
        ///     The X coordinate of center of the circle from which this piece is cut.
        /// </summary>
        public double CenterX
        {
            get { return (double) GetValue(CenterXProperty); }
            set { SetValue(CenterXProperty, value); }
        }

        public static readonly DependencyProperty CenterYProperty =
            DependencyProperty.Register("CenterYProperty", typeof(double), typeof(ChartPartShape),
                new FrameworkPropertyMetadata(0.0,
                    FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.AffectsMeasure));

        /// <summary>
        ///     The Y coordinate of center of the circle from which this pie piece is cut.
        /// </summary>
        public double CenterY
        {
            get { return (double) GetValue(CenterYProperty); }
            set { SetValue(CenterYProperty, value); }
        }

        public static readonly DependencyProperty PercentageProperty =
            DependencyProperty.Register("PercentageProperty", typeof(double), typeof(ChartPartShape),
                new FrameworkPropertyMetadata(0.0));

        /// <summary>
        ///     The percentage of a FULL circle that this piece occupies expressed as a fraction.
        ///     Setting this property changes the value of WedgeAngleProperty (and vice-versa).
        /// </summary>
        public double Percentage
        {
            get { return (double) GetValue(PercentageProperty); }
            set
            {
                SetValue(PercentageProperty, value);
                SetValue(WedgeAngleProperty, value*360.0);
            }
        }

        #endregion // Dependency properties
    }
}