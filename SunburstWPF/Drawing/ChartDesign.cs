﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using SunburstWPF.ChartModel;

namespace SunburstWPF.Drawing
{
    // changing the root of the tree
    // causes replacement of the ChartDesign object
    public class ChartDesign
    {
        private int deepestDrawnLevel;
        private List<ChartPart> drawn = new List<ChartPart>();
        private GeneralChartDesign generalChartDesign;

        public ChartDesign(List<NodeType> toDraw, double rootNodesSum, GeneralChartDesign generalChartDesign,
            Grid chartGrid)
        {
            this.generalChartDesign = generalChartDesign;
            deepestDrawnLevel = DeepestGivenRootsLevel(toDraw);
            this.generalChartDesign.Zoom = CorrectZoom(generalChartDesign.Zoom);
                // if value in the file was too little, it's corrected
            SubscribeOnPropertyChanges();
            GenerateParts(toDraw, deepestDrawnLevel, rootNodesSum, chartGrid);
            Draw();
        }

        // returns passed value parameter if that value of value is possible, i.e. does not cause the ring width of the ChartPartShapes to be negative
        // if this value is not possible returns minimum possible value
        public int CorrectZoom(int zoom)
        {
            if (zoom < 1) // should not happen (see Sunburst.xsd, <value> tag constraint), but if it does...
                zoom = 1; // minimal value value is 1
            while (RingWidth(GeneralChartDesign.ZoomScale(generalChartDesign.TotalRadius, zoom),
                       GeneralChartDesign.ZoomScale(generalChartDesign.InnerRadius, zoom),
                       deepestDrawnLevel, generalChartDesign.ChartBorders.WidthPx) <= 0.0)
                zoom++;
            return zoom;
        }

        private static double RingWidth(double zoomedTotalRadius, double zoomedInnerRadius, int deepestDrawnLevel,
            double borderWidthPx)
        {
            return Math.Floor((zoomedTotalRadius - zoomedInnerRadius)/deepestDrawnLevel);
        }

        private void Draw()
        {
            foreach (ChartPart part in drawn)
                part.DrawWithChildren();
        }

        private void GenerateParts(List<NodeType> rootsToDraw, int deepestLevel, double rootNodesSum, Grid chartGrid)
        {
            int numberOfRootsToDraw = rootsToDraw.Count;
            int rootsLevel = rootsToDraw[0].NestingLevel;
                // I assume rootsToDraw are not empty (see ChartDesigner.GetRootNodesToDraw() 
            // and definition of <data> in SunburstSchema.xsd (at least one child must be present))
            double startAngle = generalChartDesign.RotationClockwise;

            drawn.Clear();
            for (int i = 0; i < numberOfRootsToDraw; i++)
            {
                if (i > 0)
                    startAngle += drawn[i - 1].PartWedgeAngle;
                drawn.Add(new ChartPart(rootsToDraw[i], deepestLevel, startAngle, rootNodesSum, rootsLevel, RingWidth,
                    Visibility.Visible, generalChartDesign, chartGrid));
            }
        }

        private void SubscribeOnPropertyChanges()
        {
            generalChartDesign.PropertyChanged += OnRotationAngleChanged;
        }

        private void OnRotationAngleChanged(object obj, PropertyChangedEventArgs args)
        {
            switch (args.PropertyName)
            {
                case "RotationClockwise":
                    RefreshAngles();
                    break;
                default:
                    break;
            }
        }

        private void RefreshAngles()
        {
            int numberOfRootsDrawn = drawn.Count;
            double startAngle = generalChartDesign.RotationClockwise;

            for (int i = 0; i < numberOfRootsDrawn; i++)
            {
                if (i > 0)
                    startAngle += drawn[i - 1].PartWedgeAngle;
                drawn[i].ChangeAngleTo(startAngle);
            }
        }

        // coputes relative deepest level among given root node(s)
        private int DeepestGivenRootsLevel(List<NodeType> rootsToDraw)
        {
            int maxLevel = 0;
            int currentNodeDeepestLevel = 0;
            foreach (NodeType node in rootsToDraw)
            {
                currentNodeDeepestLevel = node.DeepestLevel(node.NestingLevel) - node.NestingLevel + 1;
                if (currentNodeDeepestLevel > maxLevel)
                    maxLevel = currentNodeDeepestLevel;
            }
            return maxLevel;
        }
    }
}