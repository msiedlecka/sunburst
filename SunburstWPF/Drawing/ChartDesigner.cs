﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using SunburstWPF.ChartModel;
using SunburstWPF.Converters;
using SunburstWPF.Helpers;

namespace SunburstWPF.Drawing
{
    public class ChartDesigner : PropertyChangedNotifier
    {
        public ChartDesigner(Chart chart, Grid chartGrid)
        {
            this.chart = chart;
            this.chartGrid = chartGrid;

            SubscribeOnPropertyChanges();
            ComputeMissingRadiuses();
            SetTitleDescriptionLegend(chart);
            SetBackground();
            BindChartSpace();
            RefreshDesign(chart);
        }

        #region Properties

        public List<NodeType> RootsToDraw
        {
            get { return GetRootNodesToDraw(); }
        }

        #endregion // end of Properties

        #region Fields

        private Chart chart;
        private Grid chartGrid;

        private ChartDesign design;

        #endregion // end of Fields

        #region Methods

        #region Design Computations

        public int PossibleMinimumZoom(int zoom)
        {
            return design.CorrectZoom(zoom);
        }

        private void ComputeMissingRadiuses()
        {
            if (chart.GeneralChartDesign.TotalRadius == 0)
                chart.GeneralChartDesign.TotalRadius = ComputeTotalRadius(chart.GeneralChartDesign.Zoom);

            if (chart.GeneralChartDesign.InnerRadius == 0)
                chart.GeneralChartDesign.InnerRadius =
                    ComputeInnerRadius(chart.GeneralChartDesign.TotalRadius, chart.DeepestLevel);
        }

        private double ComputeTotalRadius(int zoom)
        {
            double diameter = Math.Min(chartGrid.RowDefinitions[2].ActualHeight, chartGrid.ActualWidth)/1.3;
            return Math.Ceiling((diameter/2.0)/(zoom/100.0));
        }

        private double ComputeInnerRadius(double totalRadius, int numberOfLevels)
        {
            return Math.Floor(totalRadius/(numberOfLevels + 1));
        }

        #endregion // end of Design Computations

        #region Bindings

        private void SetTitleDescriptionLegend(Chart chart)
        {
            SetTextBlockBindings("ChartTitle", chart.Title);
            SetTextBlockBindings("ChartDescription", chart.Description);
            SetTextBlockBindings("ChartLegend", chart.Description, false);
        }

        private void SetTextBlockBindings(string textBlockName, StyledText textStyle, bool bindSpecific = true)
        {
            TextBlock textBlock = chartGrid.FindName(textBlockName) as TextBlock;
            if (textBlock != null)
            {
                BindTextBlockStyle(textBlock, textStyle);
                textBlock.ContextMenu = ContextMenuBuilder.FontChangeMenu(textStyle.Font);
                if (bindSpecific)
                    BindSpecificTopLevelTextBlock(textBlock, textStyle);
            }
        }

        private void SetBackground()
        {
            Binder.SetNewBinding("Color", chart.GeneralChartDesign.BackgroundColor, chartGrid, Grid.BackgroundProperty,
                new ColorToBrushConverter());
        }

        private void BindTextBlockStyle(TextBlock tb, StyledText styledText)
        {
            Binder.SetNewBinding("Family", styledText.Font, tb, TextBlock.FontFamilyProperty);
            Binder.SetNewBinding("Size", styledText.Font, tb, TextBlock.FontSizeProperty);
            Binder.SetNewBinding("Color", styledText.Font.Color, tb, TextBlock.ForegroundProperty,
                new ColorToBrushConverter());
            Binder.SetNewBinding("FontStyle", styledText.Font, tb, TextBlock.FontStyleProperty);
            Binder.SetNewBinding("FontStretch", styledText.Font, tb, TextBlock.FontStretchProperty);
            Binder.SetNewBinding("FontWeight", styledText.Font, tb, TextBlock.FontWeightProperty);
        }

        private void BindSpecificTopLevelTextBlock(TextBlock tb, StyledText styledText)
        {
            Binder.SetNewBinding("Value", styledText, tb, TextBlock.TextProperty);
        }

        private void BindChartSpace()
        {
            Binder.SetNewBinding("ZoomedTotalDiameter", chart.GeneralChartDesign, chartGrid.ColumnDefinitions[0],
                ColumnDefinition.MinWidthProperty);
            Binder.SetNewBinding("ZoomedTotalDiameter", chart.GeneralChartDesign, chartGrid.RowDefinitions[2],
                RowDefinition.MinHeightProperty);
        }

        #endregion // end of Bindings

        #region Chart Design Creation

        private void RefreshDesign(Chart chart)
        {
            RaisePropertyChanged("RootsToDraw");
            RemoveOldChartParts();
            RemoveOldLabels();
            CreateDesign();
        }

        private void CreateDesign()
        {
            List<NodeType> rootsToDraw = GetRootNodesToDraw();
            double rootsSum = rootsToDraw.Sum(node => { return node.Number; });
            design = new ChartDesign(rootsToDraw, rootsSum, chart.GeneralChartDesign, chartGrid);
        }

        private List<NodeType> GetRootNodesToDraw()
        {
            List<NodeType> list = new List<NodeType>();
            if (chart.GeneralChartDesign.CurrentRootId != 0)
            {
                list.Add(chart.GetNodeById(chart.GeneralChartDesign.CurrentRootId));
                return list;
            }
            return chart.Data;
        }

        #endregion // end of Chart Design Creation

        #region Clearing Chart Space

        private void RemoveOldChartParts()
        {
            RemoveElementsFromPanel<ChartPartShape>(chartGrid);
        }

        private void RemoveOldLabels()
        {
            List<string> titleDescriptionTextBlockNames = new List<string>
            {
                "ChartTitle",
                "ChartDescription",
                "ChartLegend"
            };
            RemoveElementsFromPanel<TextBlock>(chartGrid, titleDescriptionTextBlockNames);
                // clears possible abbreviations
        }

        private void RemoveElementsFromPanel<TControl>(Panel panel, List<string> namesToPreserve = null)
            where TControl : FrameworkElement
        {
            List<TControl> elementsToRemove = panel.Children.OfType<TControl>().ToList();
            foreach (TControl element in elementsToRemove)
                if (namesToPreserve != null)
                {
                    if (!namesToPreserve.Contains(element.Name))
                        panel.Children.Remove(element);
                }
                else panel.Children.Remove(element);
        }

        #endregion // end of Clearing Chart Space

        #region Property Changes Subscription

        private void SubscribeOnPropertyChanges()
        {
            chart.GeneralChartDesign.PropertyChanged += OnChartRootChanged;
        }

        private void OnChartRootChanged(object obj, PropertyChangedEventArgs args)
        {
            switch (args.PropertyName)
            {
                case "CurrentRootId":
                    RefreshDesign(chart);
                    break;
                default:
                    break;
            }
        }

        #endregion // end of Property Changes Subscription

        #endregion // end of Methods
    }
}