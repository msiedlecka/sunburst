﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using SunburstWPF.ChartModel;
using SunburstWPF.Converters;
using SunburstWPF.Helpers;

namespace SunburstWPF.Drawing
{
    public class ChartPart : PropertyChangedNotifier
    {
        public ChartPart(NodeType node, int deepestLevel, double startAngle, double rootNodesSum, int rootNestingLevel,
            Func<double, double, int, double, double> computeRing, Visibility visibility,
            GeneralChartDesign generalChartDesign, Grid chartGrid)
        {
            this.node = node;
            deepestDrawnChartLevel = deepestLevel;
            this.startAngle = startAngle;
            this.rootNodesSum = rootNodesSum;
            rootLevel = rootNestingLevel;
            this.computeRing = computeRing;
            PartVisibility = visibility;
            this.generalChartDesign = generalChartDesign;
            this.chartGrid = chartGrid;
            SubscribeOnPropertyChanges(chartGrid);
            SetBindings();
            AddChildren(chartGrid);
            Grid.SetColumn(shape, 0);
            Grid.SetRow(shape, 2);
            shape.ContextMenu = ContextMenuBuilder.MenuForChartPart(node, generalChartDesign);
            SetToolTip();
        }

        #region Fields

        private NodeType node;
        private ChartPartShape shape = new ChartPartShape();
        private List<ChartPart> children = new List<ChartPart>();

        private Grid chartGrid;
        private int deepestDrawnChartLevel;
        private GeneralChartDesign generalChartDesign;
        private double startAngle;
        private double rootNodesSum;
        private int rootLevel;
        private Func<double, double, int, double, double> computeRing;
        private Visibility visibility;

        #endregion // end of Fields

        #region Computed Properties

        private double RingWidth
        {
            get
            {
                double computed = computeRing(generalChartDesign.ZoomedTotalRadius, generalChartDesign.ZoomedInnerRadius,
                    deepestDrawnChartLevel, generalChartDesign.ChartBorders.WidthPx);
                if (computed <= 0.0)
                    return 1.0; // quick workaround for crashes; protect zoom
                return computed;
            }
        }

        public double PartInnerRadius
        {
            get { return generalChartDesign.ZoomedInnerRadius + (node.NestingLevel - rootLevel)*RingWidth; }
        }

        public double PartTotalRadius
        {
            get { return PartInnerRadius + RingWidth; }
        }

        public double CenterX
        {
            get
            {
                return Math.Max(ChartColumnDefinition.ActualWidth/2.0 + SettingsHelper.GetChartMargin().Left,
                    generalChartDesign.ZoomedTotalRadius);
            }
        }

        public double CenterY
        {
            get
            {
                return Math.Max(ChartRowDefinition.ActualHeight/2.0 + SettingsHelper.GetChartMargin().Left,
                    generalChartDesign.ZoomedTotalRadius);
            }
        }

        public double PartStartAngle
        {
            get { return startAngle; }
            private set
            {
                startAngle = value;
                RaisePropertyChanged("PartStartAngle");
            }
        }

        public double PartWedgeAngle
        {
            get { return PartPercentage*360.0; }
        }

        public double PartPercentage
        {
            get { return node.Number/rootNodesSum; }
        }

        public Visibility PartVisibility
        {
            get { return visibility; }
            set
            {
                if (!visibility.Equals(value))
                {
                    visibility = value;
                    RaisePropertyChanged("PartVisibility");
                }
            }
        }

        #endregion // end of Computed Properties

        #region Private Properties

        // VERY UGLY, totally dependent from layout, the sense for keeping rowdefinitions' names in Settings just died
        private RowDefinition ChartRowDefinition
        {
            get { return chartGrid.RowDefinitions[2]; }
        }

        private ColumnDefinition ChartColumnDefinition
        {
            get { return chartGrid.ColumnDefinitions[0]; }
        }

        #endregion // end of Private Properties

        #region Methods

        public void DrawWithChildren()
        {
            chartGrid.Children.Add(shape);
            foreach (ChartPart child in children)
                child.DrawWithChildren();
        }

        private void ReorderChildren()
        {
            List<ChartPart> newChildren = new List<ChartPart>(children.Count);
            int indexOfMatching = 0;

            foreach (NodeType nodeChild in node.ChildNodes)
            {
                indexOfMatching = children.FindIndex(part => { return part.node.Id == nodeChild.Id; });
                newChildren.Add(children[indexOfMatching]);
                children.RemoveAt(indexOfMatching);
            }

            children = newChildren;
        }

        private void ChangeChildrenVisibility(Visibility newVisibility)
        {
            Visibility childrenVisibility = newVisibility;

            // if current node is not expanded, it's children remain 
            // invisible no matter what newVisibility's value was
            if (!node.Expanded)
                childrenVisibility = Visibility.Hidden;

            foreach (ChartPart child in children)
            {
                child.PartVisibility = childrenVisibility;
                child.ChangeChildrenVisibility(childrenVisibility);
            }
        }

        private void AddChildren(Grid chartGrid)
        {
            int childrenCount = node.ChildNodes.Count;
            double childStartAngle = PartStartAngle;
            Visibility childrenVisibility = Visibility.Visible;

            if (PartVisibility.Equals(Visibility.Hidden) || !node.Expanded)
                childrenVisibility = Visibility.Hidden;

            for (int i = 0; i < childrenCount; i++)
            {
                if (i > 0)
                    childStartAngle += children[i - 1].PartWedgeAngle;
                children.Add(new ChartPart(node.ChildNodes[i], deepestDrawnChartLevel, childStartAngle, rootNodesSum,
                    rootLevel, computeRing, childrenVisibility, generalChartDesign, chartGrid));
            }
        }

        public void ChangeAngleTo(double newStartAngle)
        {
            int childrenCount = node.ChildNodes.Count;
            double childStartAngle = newStartAngle;

            PartStartAngle = newStartAngle;

            for (int i = 0; i < childrenCount; i++)
            {
                if (i > 0)
                    childStartAngle += children[i - 1].PartWedgeAngle;
                children[i].ChangeAngleTo(childStartAngle);
            }
        }

        private void SetToolTip()
        {
            ToolTip tip = new ToolTip();
            tip.DataContext = node;
            shape.ToolTip = tip;
        }

        private void SetBindings()
        {
            Binder.SetBinding("PartTotalRadius", this, shape, ChartPartShape.TotalRadiusProperty);
            Binder.SetBinding("PartInnerRadius", this, shape, ChartPartShape.InnerRadiusProperty);
            Binder.SetBinding("CenterX", this, shape, ChartPartShape.CenterXProperty);
            Binder.SetBinding("CenterY", this, shape, ChartPartShape.CenterYProperty);
            Binder.SetBinding("PartStartAngle", this, shape, ChartPartShape.RotationAngleProperty);
            Binder.SetBinding("PartPercentage", this, shape, ChartPartShape.PercentageProperty);
            Binder.SetBinding("PartWedgeAngle", this, shape, ChartPartShape.WedgeAngleProperty);
            Binder.SetBinding("PartVisibility", this, shape, ChartPartShape.VisibilityProperty);
            Binder.SetBinding("Color", node.Color, shape, ChartPartShape.FillProperty, new ColorToBrushConverter());
            Binder.SetBinding("Color", generalChartDesign.ChartBorders.Color, shape, ChartPartShape.StrokeProperty,
                new ColorToBrushConverter());
            Binder.SetBinding("WidthPx", generalChartDesign.ChartBorders, shape, ChartPartShape.StrokeThicknessProperty);
        }

        #region Property Changes Subscription

        private void SubscribeOnPropertyChanges(Grid chartGrid)
        {
            generalChartDesign.PropertyChanged += OnChartPropertyChanged;
            generalChartDesign.ChartBorders.PropertyChanged += OnChartPropertyChanged;
            node.PropertyChanged += OnNodePropertyChanged;
            chartGrid.SizeChanged += ChartGridSizeChanged;
        }

        private void ChartGridSizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (e.HeightChanged)
                RaisePropertyChanged("CenterY");
            if (e.WidthChanged)
                RaisePropertyChanged("CenterX");
        }

        private void OnChartPropertyChanged(object obj, PropertyChangedEventArgs args)
        {
            switch (args.PropertyName)
            {
                case "TotalRadius":
                case "InnerRadius":
                case "Zoom":
                case "ChartBorders":
                case "WidthPx":
                    RaisePropertyChanged("PartTotalRadius");
                    RaisePropertyChanged("PartInnerRadius");
                    RaisePropertyChanged("RingWidth");
                    RaisePropertyChanged("CenterX");
                    RaisePropertyChanged("CenterY");
                    break;
                default:
                    break;
            }
        }

        private void OnNodePropertyChanged(object obj, PropertyChangedEventArgs args)
        {
            switch (args.PropertyName)
            {
                case "Expanded":
                    ChangeChildrenVisibility(node.Expanded ? Visibility.Visible : Visibility.Collapsed);
                    break;
                case "ChildNodes":
                    ReorderChildren();
                    ChangeAngleTo(PartStartAngle);
                    break;
                default:
                    break;
            }
        }

        #endregion // end of Property Changes Subscription

        #endregion // end of Methods
    }
}