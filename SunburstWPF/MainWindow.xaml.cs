﻿using System.Windows;
using SunburstWPF.ViewModels;

namespace SunburstWPF
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            MainViewModel mvm = new MainViewModel(ChartViewer, MainToolbar, UseAbbreviationsMenuItem);
            mvm.RequestClose += (obj, args) =>
            {
                Closing -= mvm.OnMainWindowClosing;
                Close();
            };
            Closing += mvm.OnMainWindowClosing;
            DataContext = mvm;
        }
    }
}