﻿using System.Windows;
using SunburstWPF.Helpers;

namespace SunburstWPF
{
    /// <summary>
    ///     Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            SettingsHelper.ActivateCurrentLanguage();
            base.OnStartup(e);
        }
    }
}