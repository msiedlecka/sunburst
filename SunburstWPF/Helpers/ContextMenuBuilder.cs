﻿using System.Windows.Controls;
using SunburstWPF.ChartModel;
using SunburstWPF.Commands.PresentationChange;
using SunburstWPF.Commands.Subview;
using SunburstWPF.Languages;

namespace SunburstWPF.Helpers
{
    public class ContextMenuBuilder
    {
        public static ContextMenu MenuForChartPart(NodeType node, GeneralChartDesign generalDesign)
        {
            ContextMenu contextMenu = new ContextMenu();
            contextMenu.Items.Add(ExpandedItem(node));
            contextMenu.Items.Add(SetAsRootItem(node, generalDesign));
            contextMenu.Items.Add(ChangeChildrenOrderItem(node));
            contextMenu.Items.Add(ChangeColorItem(node));
            return contextMenu;
        }

        public static ContextMenu FontChangeMenu(FontDefinitionType font)
        {
            ContextMenu contextMenu = new ContextMenu();
            contextMenu.Items.Add(ChangeFontItem(font));
            return contextMenu;
        }

        private static MenuItem ChangeFontItem(FontDefinitionType font)
        {
            MenuItem changeFontItem = new MenuItem();
            ChangeFontStyleCommand changeFontCommand = new ChangeFontStyleCommand(font, Lang.SetFontProperties);
            changeFontItem.Command = changeFontCommand;
            changeFontItem.Header = changeFontCommand.Text;
            return changeFontItem;
        }

        private static MenuItem ChangeColorItem(NodeType node)
        {
            MenuItem changeColorItem = new MenuItem();
            ChangeColorCommand changeColorCommand = new ChangeColorCommand(node.Color, Lang.SetColor);
            changeColorItem.Command = changeColorCommand;
            changeColorItem.Header = changeColorCommand.Text;
            return changeColorItem;
        }

        private static MenuItem ChangeChildrenOrderItem(NodeType node)
        {
            MenuItem changeOrderItem = new MenuItem();
            ChangeChildrenOrderCommand changeOrderCommand = new ChangeChildrenOrderCommand(node);
            changeOrderItem.Command = changeOrderCommand;
            changeOrderItem.Header = changeOrderCommand.Text;
            return changeOrderItem;
        }

        private static MenuItem ExpandedItem(NodeType node)
        {
            MenuItem expandedItem = new MenuItem();
            expandedItem.Header = Lang.Expanded;
            expandedItem.IsCheckable = true;
            Binder.SetNewBinding("Expanded", node, expandedItem, MenuItem.IsCheckedProperty);
            return expandedItem;
        }

        private static MenuItem SetAsRootItem(NodeType node, GeneralChartDesign generalDesign)
        {
            MenuItem setAsRootItem = new MenuItem();
            SetAsRootCommand setAsRootCommand = new SetAsRootCommand(Lang.SetAsRoot, node.Id, generalDesign);
            setAsRootItem.Command = setAsRootCommand;
            setAsRootItem.Header = setAsRootCommand.Text;
            return setAsRootItem;
        }
    }
}