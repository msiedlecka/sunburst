﻿using System.Collections.Specialized;
using System.Text;

namespace SunburstWPF.Helpers
{
    public class FileDialogHelper
    {
        private const string filterDelimiter = "|";

        public static string CreateExtensionsFilter(StringCollection extensionsList)
        {
            StringBuilder filterBuilder = new StringBuilder();

            foreach (string extension in extensionsList)
                filterBuilder.Append(extension + filterDelimiter);
            filterBuilder.Remove(filterBuilder.Length - 1, 1); // removes last delimiter that is not needed

            return filterBuilder.ToString();
        }

        // default extension is the first at the list of supported ones
        public static string GetDefaultExtension(StringCollection extensionsList)
        {
            string defaultExtension = extensionsList.Count > 0 ? extensionsList[0] : "";
            return defaultExtension;
        }
    }
}