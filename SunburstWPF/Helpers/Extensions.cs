﻿using System.Collections.Generic;

namespace ExtensionMethods
{
    public static class Extensions
    {
        public static void Swap<T>(this IList<T> list, int firstIndex, int secondIndex)
        {
            if (list != null && firstIndex >= 0 && firstIndex < list.Count && secondIndex >= 0 &&
                secondIndex < list.Count)
                if (firstIndex != secondIndex)
                {
                    T temp = list[firstIndex];
                    list[firstIndex] = list[secondIndex];
                    list[secondIndex] = temp;
                }
        }
    }
}