﻿using System.Windows;
using System.Windows.Data;

namespace SunburstWPF.Helpers
{
    public class Binder
    {
        public static void SetBinding(string propertyName, object source, DependencyObject element,
            DependencyProperty dependencyProperty,
            IValueConverter converter = null, BindingMode mode = BindingMode.Default)
        {
            Binding binding = new Binding(propertyName);
            binding.Source = source;
            binding.Converter = converter;
            binding.Mode = mode;
            BindingOperations.SetBinding(element, dependencyProperty, binding);
        }

        public static void UnbindAll(DependencyObject obj)
        {
            BindingOperations.ClearAllBindings(obj);
        }

        public static void Unbind(DependencyObject obj, DependencyProperty property)
        {
            BindingOperations.ClearBinding(obj, property);
        }

        public static void SetNewBinding(string propertyName, object source, DependencyObject element,
            DependencyProperty dependencyProperty,
            IValueConverter converter = null, BindingMode mode = BindingMode.Default)
        {
            Unbind(element, dependencyProperty);
            SetBinding(propertyName, source, element, dependencyProperty, converter);
        }
    }
}