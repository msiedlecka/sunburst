﻿using System.Collections.Specialized;

namespace SunburstWPF.Helpers
{
    public class ExtensionsHelper
    {
        public static string argumentExceptionDescription = "Files of selected format are not supported.";

        public static string notImplementedExceptionDescription =
            "Files of selected format will be supported, but this has not been implemented yet.";

        public static bool IsExtensionSupported(string fileExtension, StringCollection extensionList)
        {
            foreach (string filterEntry in extensionList)
                if (filterEntry.Contains(fileExtension))
                    return true;
            return false;
        }
    }
}