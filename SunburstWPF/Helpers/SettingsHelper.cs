﻿using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Globalization;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using SunburstWPF.ChartModel;
using SunburstWPF.Properties;

namespace SunburstWPF.Helpers
{
    public class SettingsHelper
    {
        public static FontDefinitionType GetTitleFont()
        {
            FontDefinitionType title = GetDefaultFont();
            title.Size = Settings.Default.DefaultTitleSize;
            return title;
        }

        public static FontDefinitionType GetDescriptionFont()
        {
            FontDefinitionType desc = GetDefaultFont();
            desc.Size = Settings.Default.DefaultDescriptionSize;
            return desc;
        }

        public static FontDefinitionType GetLabelFont()
        {
            return GetDefaultFont();
        }

        public static Color GetDefaultBackgroundColor()
        {
            return Settings.Default.DefaultBackgroundColor;
        }

        public static LineStyleType GetLinesStyle()
        {
            Color color = Settings.Default.DefaultLineColor;
            double width = Settings.Default.DefaultLineWidth;
            return new LineStyleType(color, width);
        }

        public static StringCollection GetPalette()
        {
            return Settings.Default.DefaultPalette;
            ;
        }

        public static int GetZoom()
        {
            return Settings.Default.DefaultZoom;
        }

        public static double GetRotationAngle()
        {
            return Settings.Default.DefaultRotation;
        }

        public static double GetLightenPercentageStep()
        {
            return Settings.Default.LightenPercentageStep;
        }

        public static Thickness GetChartMargin()
        {
            return Settings.Default.ChartMargin;
        }

        public static string GetDefaultFileName()
        {
            return Settings.Default.DefaultFileName;
        }

        public static StringCollection GetSupportedExtensions()
        {
            return Settings.Default.SupportedExtensions;
        }

        public static StringCollection GetSupportedImageExtensions()
        {
            return Settings.Default.SupportedImageExtensions;
        }

        public static CultureInfo GetCurrentLanguage()
        {
            return Settings.Default.DefaultLanguage;
        }

        public static void SetCurrentLanguage(CultureInfo culture)
        {
            Settings.Default.DefaultLanguage = culture;
        }

        public static void ActivateCurrentLanguage()
        {
            Thread.CurrentThread.CurrentCulture = GetCurrentLanguage();
            Thread.CurrentThread.CurrentUICulture = GetCurrentLanguage();
        }

        public static void SaveSettings()
        {
            Settings.Default.Save();
        }

        public static ObservableCollection<CultureInfo> GetSupportedLanguages()
        {
            StringCollection supportedLanguagesList = Settings.Default.SupportedLanguages;
            ObservableCollection<CultureInfo> culturesList = new ObservableCollection<CultureInfo>();
            foreach (string cultureName in supportedLanguagesList)
                culturesList.Add(new CultureInfo(cultureName));
            return culturesList;
        }

        public static int GetZoomChangeButtonStep()
        {
            return Settings.Default.ZoomChangeButtonStep;
        }

        public static double GetAngleChangeButtonStep()
        {
            return Settings.Default.AngleChangeStep;
        }

        public static string GetChartGridName()
        {
            return Settings.Default.ChartGridName;
        }

        public static string GetTitleRowName()
        {
            return Settings.Default.TitleRowName;
        }

        public static string GetDescriptionRowName()
        {
            return Settings.Default.DescriptionRowName;
        }

        public static string GetChartRowName()
        {
            return Settings.Default.ChartRowName;
        }

        public static string GetLeftLabelsColumnName()
        {
            return Settings.Default.LeftLabelsColumnName;
        }

        public static string GetRightLabelsColumnName()
        {
            return Settings.Default.RightLabelsColumnName;
        }

        public static string GetChartColumnName()
        {
            return Settings.Default.ChartColumnName;
        }

        // common for all fonts
        private static FontDefinitionType GetDefaultFont()
        {
            FontDefinitionType font = new FontDefinitionType();
            font.Family = Settings.Default.DefaultFontFamily;
            font.FontStyle = Settings.Default.DefaultFontStyle;
            font.FontStretch = Settings.Default.DefaultFontStretch;
            font.FontWeight = Settings.Default.DefaultFontWeight;
            font.Color = new ARGBColor(Settings.Default.DefaultTextColor);
            font.Size = Settings.Default.DefaultLabelSize; // so that it's not zero ;)
            return font;
        }
    }
}