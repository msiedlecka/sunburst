﻿<Window x:Class="SunburstWPF.MainWindow"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:l="clr-namespace:SunburstWPF.Languages"
        xmlns:p="clr-namespace:SunburstWPF.Properties"
        xmlns:xctk="http://schemas.xceed.com/wpf/xaml/toolkit"
        xmlns:converters="clr-namespace:SunburstWPF.Converters"
        xmlns:chart="clr-namespace:SunburstWPF.ChartModel"
        Title="Sunburst" Height="750" Width="1000" MinHeight="450" MinWidth="600"
        Icon="Resources/silver-sun-48.png">
    <Window.Resources>
        <converters:IsNotNullConverter x:Key="IsNotNullConverter" />
        <converters:ColorToBrushConverter x:Key="ColorToBrushConverter" />
        <converters:BoolToVisibilityConverter x:Key="BoolToVisibilityConverter" />
    </Window.Resources>
    <Window.InputBindings>
        <KeyBinding Command="{Binding OpenCommand}" Key="{Binding OpenCommand.Gesture.Key}"
                    Modifiers="{Binding OpenCommand.Gesture.Modifiers}" />
        <KeyBinding Command="{Binding SaveCommand}" Key="{Binding SaveCommand.Gesture.Key}"
                    Modifiers="{Binding SaveCommand.Gesture.Modifiers}" />
        <KeyBinding Command="{Binding SaveAsCommand}" Key="{Binding SaveAsCommand.Gesture.Key}"
                    Modifiers="{Binding SaveAsCommand.Gesture.Modifiers}" />
        <KeyBinding Command="{Binding SaveAsImageCommand}" Key="{Binding SaveAsImageCommand.Gesture.Key}"
                    Modifiers="{Binding SaveAsImageCommand.Gesture.Modifiers}" />
        <KeyBinding Command="{Binding CloseCommand}" Key="{Binding CloseCommand.Gesture.Key}"
                    Modifiers="{Binding CloseCommand.Gesture.Modifiers}" />
        <KeyBinding Command="{Binding DecrementZoomCommand}" Key="{Binding DecrementZoomCommand.Gesture.Key}"
                    Modifiers="{Binding DecrementZoomCommand.Gesture.Modifiers}" />
        <KeyBinding Command="{Binding IncrementZoomCommand}" Key="{Binding IncrementZoomCommand.Gesture.Key}"
                    Modifiers="{Binding IncrementZoomCommand.Gesture.Modifiers}" />
        <KeyBinding Command="{Binding GoLevelUpCommand}" Key="{Binding GoLevelUpCommand.Gesture.Key}"
                    Modifiers="{Binding GoLevelUpCommand.Gesture.Modifiers}" />
        <KeyBinding Command="{Binding GoToRootCommand}" Key="{Binding GoToRootCommand.Gesture.Key}"
                    Modifiers="{Binding GoToRootCommand.Gesture.Modifiers}" />
        <KeyBinding Command="{Binding IncreaseAngleCommand}" Key="{Binding IncreaseAngleCommand.Gesture.Key}"
                    Modifiers="{Binding IncreaseAngleCommand.Gesture.Modifiers}" />
        <KeyBinding Command="{Binding DecreaseAngleCommand}" Key="{Binding DecreaseAngleCommand.Gesture.Key}"
                    Modifiers="{Binding DecreaseAngleCommand.Gesture.Modifiers}" />
    </Window.InputBindings>
    <Grid>
        <Grid.RowDefinitions>
            <RowDefinition Height="Auto" />
            <RowDefinition Height="Auto" />
            <RowDefinition Height="*" />
        </Grid.RowDefinitions>
        <Menu IsMainMenu="True" Grid.Row="0">
            <MenuItem Header="{x:Static l:Lang.File}">
                <MenuItem Header="{Binding OpenCommand.Text}" Command="{Binding OpenCommand}"
                          InputGestureText="{Binding OpenCommand.GestureText}">
                    <MenuItem.Icon>
                        <Image Source="Resources/folder_open.png" Style="{StaticResource MenuIcon}" />
                    </MenuItem.Icon>
                </MenuItem>
                <MenuItem Header="{Binding SaveCommand.Text}" Command="{Binding SaveCommand}"
                          InputGestureText="{Binding SaveCommand.GestureText}">
                    <MenuItem.Icon>
                        <Image Source="Resources/save.png" Style="{StaticResource MenuIcon}" />
                    </MenuItem.Icon>
                </MenuItem>
                <MenuItem Header="{Binding SaveAsCommand.Text}" Command="{Binding SaveAsCommand}"
                          InputGestureText="{Binding SaveAsCommand.GestureText}">
                    <MenuItem.Icon>
                        <Image Source="Resources/save-as.png" Style="{StaticResource MenuIcon}" />
                    </MenuItem.Icon>
                </MenuItem>
                <MenuItem Header="{Binding SaveAsImageCommand.Text}" Command="{Binding SaveAsImageCommand}"
                          InputGestureText="{Binding SaveAsImageCommand.GestureText}">
                    <MenuItem.Icon>
                        <Image Source="Resources/camera.jpg" Style="{StaticResource MenuIcon}" />
                    </MenuItem.Icon>
                </MenuItem>
                <MenuItem Header="{Binding CloseCommand.Text}" Command="{Binding CloseCommand}"
                          InputGestureText="{Binding CloseCommand.GestureText}">
                    <MenuItem.Icon>
                        <Image Source="Resources/icon_close.png" Style="{StaticResource MenuIcon}" />
                    </MenuItem.Icon>
                </MenuItem>
            </MenuItem>
            <MenuItem Header="{x:Static l:Lang.Edit}">
                <MenuItem Header="{x:Static l:Lang.SetFontProperties}">
                    <MenuItem.Icon>
                        <Image Source="Resources/black_font.png" Style="{StaticResource MenuIcon}" />
                    </MenuItem.Icon>
                    <MenuItem Command="{Binding ChangeTitleStyleCommand}"
                              Header="{Binding ChangeTitleStyleCommand.Text}"
                              CommandParameter="{Binding RelativeSource={RelativeSource Mode=FindAncestor, AncestorType={x:Type Window}}}" />
                    <MenuItem Command="{Binding ChangeDescriptionStyleCommand}"
                              Header="{Binding ChangeDescriptionStyleCommand.Text}"
                              CommandParameter="{Binding RelativeSource={RelativeSource Mode=FindAncestor, AncestorType={x:Type Window}}}" />
                    <MenuItem Command="{Binding ChangeLabelStyleCommand}"
                              Header="{Binding ChangeLabelStyleCommand.Text}"
                              CommandParameter="{Binding RelativeSource={RelativeSource Mode=FindAncestor, AncestorType={x:Type Window}}}" />
                </MenuItem>
                <MenuItem IsEnabled="{Binding IsChartLoaded}" Command="{Binding ChangeBackgroundCommand}"
                          Header="{Binding ChangeBackgroundCommand.Text}"
                          CommandParameter="{Binding RelativeSource={RelativeSource Mode=FindAncestor, AncestorType={x:Type Window}}}">
                    <MenuItem.Icon>
                        <Image Source="Resources/color_wheel.png" Style="{StaticResource MenuIcon}" />
                    </MenuItem.Icon>
                </MenuItem>
                <MenuItem IsEnabled="{Binding IsChartLoaded}" Command="{Binding ChangeBordersCommand}"
                          Header="{Binding ChangeBordersCommand.Text}"
                          CommandParameter="{Binding RelativeSource={RelativeSource Mode=FindAncestor, AncestorType={x:Type Window}}}">
                    <MenuItem.Icon>
                        <Image Source="Resources/SingleLine.png" Style="{StaticResource MenuIcon}" />
                    </MenuItem.Icon>
                </MenuItem>
            </MenuItem>
            <MenuItem Header="{x:Static l:Lang.View}">
                <!-- not implemented yet; should be: IsEnabled="{Binding IsChartLoaded}" -->
                <MenuItem Name="UseAbbreviationsMenuItem" Header="{x:Static l:Lang.UseAbbreviations}"
                    IsCheckable="True" IsEnabled="False" />
                <MenuItem Command="{Binding ChangeLanguageCommand}" Header="{Binding ChangeLanguageCommand.Text}"
                          CommandParameter="{Binding RelativeSource={RelativeSource Mode=FindAncestor, AncestorType={x:Type Window}}}">
                    <MenuItem.Icon>
                        <Image Source="Resources/international.png" Style="{StaticResource MenuIcon}" />
                    </MenuItem.Icon>
                </MenuItem>
            </MenuItem>
            <MenuItem Header="{x:Static l:Lang.Help}">
                <MenuItem Command="{Binding AboutCommand}" Header="{Binding AboutCommand.Text}">
                    <MenuItem.Icon>
                        <Image Source="Resources/about-icon.png" Style="{StaticResource MenuIcon}" />
                    </MenuItem.Icon>
                </MenuItem>
            </MenuItem>
        </Menu>
        <ToolBarTray Grid.Row="1" Name="MainToolbar">
            <ToolBar>
                <Button Command="{Binding OpenCommand}" ToolTip="{Binding OpenCommand.ToolTip}">
                    <Image Style="{StaticResource MenuBarIcon}" Source="Resources/folder_open_24.png" />
                </Button>
                <Button Command="{Binding SaveCommand}" ToolTip="{Binding SaveCommand.ToolTip}">
                    <Image Style="{StaticResource MenuBarIcon}" Source="Resources/save_24.png" />
                </Button>
                <Button Command="{Binding SaveAsImageCommand}" ToolTip="{Binding SaveAsImageCommand.ToolTip}">
                    <Image Style="{StaticResource MenuBarIcon}" Source="Resources/camera_24.png" />
                </Button>
            </ToolBar>
            <ToolBar IsEnabled="{Binding IsChartLoaded}">
                <Button Command="{Binding DecrementZoomCommand}" ToolTip="{Binding DecrementZoomCommand.ToolTip}">
                    <Image Style="{StaticResource MenuBarIcon}" Source="Resources/minus_24.png" />
                </Button>
                <Button Command="{Binding IncrementZoomCommand}" ToolTip="{Binding IncrementZoomCommand.ToolTip}">
                    <Image Style="{StaticResource MenuBarIcon}" Source="Resources/plus_24.png" />
                </Button>
                <Slider ToolTip="{x:Static l:Lang.SetZoom}" Minimum="1" Maximum="1000" TickPlacement="BottomRight"
                        TickFrequency="20" IsSnapToTickEnabled="True" Name="ZoomSlider" MinWidth="150"
                        IsMoveToPointEnabled="True" />
                <xctk:IntegerUpDown Name="ZoomUpDownBox" Style="{StaticResource ZoomUpDownChanger}"
                                    ToolTip="{x:Static l:Lang.SetZoom}" DefaultValue="100" />
                <Label Content="%" FontSize="14" />
            </ToolBar>
            <ToolBar>
                <Button IsEnabled="{Binding IsChartLoaded}" Command="{Binding ChangeBackgroundCommand}"
                        ToolTip="{Binding ChangeBackgroundCommand.ToolTip}" Style="{StaticResource CommandButton}">
                    <Image Style="{StaticResource MenuBarIcon}" Source="Resources/color_wheel_24.png" />
                </Button>
                <Button IsEnabled="{Binding IsChartLoaded}" Command="{Binding ChangeBordersCommand}"
                        ToolTip="{Binding ChangeBordersCommand.ToolTip}" Style="{StaticResource CommandButton}">
                    <Image Style="{StaticResource MenuBarIcon}" Source="Resources/SingleLine_24.jpg" />
                </Button>
                <StackPanel Orientation="Horizontal" IsEnabled="{Binding IsChartLoaded}">
                    <Separator Style="{StaticResource {x:Static ToolBar.SeparatorStyleKey}}" />
                    <Image Style="{StaticResource MenuBarIcon}" Source="Resources/circle_24.png"
                           ToolTip="{x:Static l:Lang.SetTotalRadiusLength}" Margin="0, 0, 2, 0" />
                    <xctk:DoubleUpDown Name="TotalRadiusUpDownBox" Style="{StaticResource DoubleUpDownChangers}"
                                       ToolTip="{x:Static l:Lang.SetTotalRadiusLength}"
                                       DefaultValue="300" Minimum="1" />
                    <Label Content="px" Margin="0, 0, 3, 0" FontSize="14" VerticalAlignment="Top" />

                    <Separator Style="{StaticResource {x:Static ToolBar.SeparatorStyleKey}}" />
                    <Image Style="{StaticResource MenuBarIcon}" Source="Resources/inner-24.png"
                           ToolTip="{x:Static l:Lang.SetInnerRadiusLength}" Margin="2, 0, 2, 0" />
                    <xctk:DoubleUpDown Name="InnerRadiusUpDownBox" Style="{StaticResource DoubleUpDownChangers}"
                                       ToolTip="{x:Static l:Lang.SetInnerRadiusLength}"
                                       DefaultValue="30" Minimum="1" />
                    <Label Content="px" Margin="0, 0, 3, 0" FontSize="14" VerticalAlignment="Top" />

                    <Separator Style="{StaticResource {x:Static ToolBar.SeparatorStyleKey}}" />
                    <Image Style="{StaticResource MenuBarIcon}" Source="Resources/angle-24.png"
                           ToolTip="{x:Static l:Lang.SetRotationAngle}" Margin="2, 0, 2, 0" />
                    <xctk:DoubleUpDown Name="AngleUpDownBox" Style="{StaticResource DoubleUpDownChangers}"
                                       ToolTip="{x:Static l:Lang.SetRotationAngle}"
                                       DefaultValue="0" Minimum="0" Maximum="359.999"
                                       Increment="{Binding Source={x:Static p:Settings.Default}, Path=AngleChangeStep}" />
                    <Label Content="°" FontSize="14" VerticalAlignment="Top" />
                </StackPanel>
            </ToolBar>
            <ToolBar IsEnabled="{Binding IsChartLoaded}">
                <Button Command="{Binding GoLevelUpCommand}" ToolTip="{Binding GoLevelUpCommand.ToolTip}">
                    <Image Style="{StaticResource MenuBarIcon}" Source="Resources/up_24.png" />
                </Button>
                <Button Command="{Binding GoToRootCommand}" ToolTip="{Binding GoToRootCommand.ToolTip}">
                    <Image Style="{StaticResource MenuBarIcon}" Source="Resources/center_24.png" />
                </Button>
                <!--not implemented yet -->
                <CheckBox Name="AbbrevCheckbox" Content="{x:Static l:Lang.UseAbbreviations}" IsEnabled="False" />
            </ToolBar>
        </ToolBarTray>
        <ScrollViewer Grid.Row="2" Margin="5" Name="ChartViewer" HorizontalScrollBarVisibility="Visible"
                      IsDeferredScrollingEnabled="True">
            <Grid ShowGridLines="False" Name="ChartGrid"
                  Background="{Binding Source={x:Static p:Settings.Default}, Path=DefaultBackgroundColor, Converter={StaticResource ColorToBrushConverter}}">
                <Grid.RowDefinitions>
                    <RowDefinition Height="Auto" Name="TitleRow" />
                    <RowDefinition Height="Auto" Name="DescriptionRow" />
                    <RowDefinition Height="*" Name="ChartRow" />
                </Grid.RowDefinitions>
                <Grid.ColumnDefinitions>
                    <ColumnDefinition Width="Auto" Name="ChartColumn" />
                    <ColumnDefinition Width="*" />
                </Grid.ColumnDefinitions>
                <StackPanel VerticalAlignment="Center" Grid.Column="2" Grid.Row="2" Margin="20,0,0,0">
                    <TextBlock Name="ChartLegend" TextAlignment="Left" Padding="5" Text="{x:Static l:Lang.Legend}"
                               Visibility="{Binding RelativeSource={RelativeSource Mode=FindAncestor, AncestorType={x:Type Window}}, 
                                Path=DataContext.IsChartLoaded, Converter={StaticResource BoolToVisibilityConverter}}" />
                    <TreeView Name="ChartLabels" Padding="2"
                              Background="{Binding ElementName=ChartGrid, Path=Background}"
                              VirtualizingStackPanel.IsVirtualizing="True"
                              ScrollViewer.VerticalScrollBarVisibility="Hidden"
                              ScrollViewer.HorizontalScrollBarVisibility="Hidden" BorderThickness="0">
                        <TreeView.Resources>
                            <SolidColorBrush x:Key="{x:Static SystemColors.ControlBrushKey}" Color="Transparent" />
                        </TreeView.Resources>
                        <TreeView.ItemTemplate>
                            <HierarchicalDataTemplate DataType="{x:Type chart:NodeType}"
                                                      ItemsSource="{Binding ChildNodes}">
                                <Grid ShowGridLines="False">
                                    <Grid.ColumnDefinitions>
                                        <ColumnDefinition Width="*" />
                                        <ColumnDefinition Width="*" />
                                    </Grid.ColumnDefinitions>
                                    <Grid Grid.Column="0" Margin="5" MinHeight="10"
                                          Width="{Binding RelativeSource={RelativeSource Self}, Path=ActualHeight}"
                                          Background="{Binding Color.Color, Converter={StaticResource ColorToBrushConverter}}">
                                        <Border
                                            BorderBrush="{Binding ElementName=ChartGrid,
					                        Path=DataContext.GeneralChartDesign.ChartBorders.Color.Color, Converter={StaticResource ColorToBrushConverter}}"
                                            BorderThickness="2" />
                                    </Grid>
                                    <TextBlock Grid.Column="1" Margin="2" Text="{Binding FullLabel}"
                                               TextWrapping="Wrap" VerticalAlignment="Center"
                                               FontFamily="{Binding ElementName=ChartGrid, Path=DataContext.GeneralChartDesign.LabelFont.Family}"
                                               FontStyle="{Binding ElementName=ChartGrid, Path=DataContext.GeneralChartDesign.LabelFont.FontStyle}"
                                               FontSize="{Binding ElementName=ChartGrid, Path=DataContext.GeneralChartDesign.LabelFont.Size}"
                                               FontWeight="{Binding ElementName=ChartGrid, Path=DataContext.GeneralChartDesign.LabelFont.FontWeight}"
                                               FontStretch="{Binding ElementName=ChartGrid, Path=DataContext.GeneralChartDesign.LabelFont.FontStretch}"
                                               Foreground="{Binding ElementName=ChartGrid,
					                        Path=DataContext.GeneralChartDesign.LabelFont.Color.Color, Converter={StaticResource ColorToBrushConverter}}" />
                                </Grid>
                            </HierarchicalDataTemplate>
                        </TreeView.ItemTemplate>
                    </TreeView>
                </StackPanel>
                <TextBlock Name="ChartTitle" Style="{StaticResource TopLevelLabel}" Grid.Row="0" Grid.Column="0"
                           Grid.ColumnSpan="2"
                           MaxWidth="{Binding ElementName=ChartGrid, Path=ActualWidth}" />
                <TextBlock Name="ChartDescription" Style="{StaticResource TopLevelLabel}" Grid.Row="1" Grid.Column="0"
                           Grid.ColumnSpan="2"
                           MaxWidth="{Binding ElementName=ChartGrid, Path=ActualWidth}" />
            </Grid>
        </ScrollViewer>
    </Grid>
</Window>