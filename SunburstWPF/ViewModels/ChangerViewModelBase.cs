﻿using System.ComponentModel;
using SunburstWPF.ChartModel;
using SunburstWPF.Commands.Subview;
using SunburstWPF.Languages;

namespace SunburstWPF.ViewModels
{
    public abstract class ChangerViewModelBase : PropertyChangedNotifier
    {
        protected ChangerViewModelBase()
        {
            OkCommand = new CloseChangerSubviewCommand(Lang.OK, OnViewClosing);
            CancelCommand = new CloseChangerSubviewCommand(Lang.Cancel, OnViewClosing);
            CancelCommand.BeforeExecuted += () => { CancelChanges(); };
        }

        protected abstract void CancelChanges();

        public void OnViewClosing(object sender, CancelEventArgs e)
        {
            CancelChanges();
        }

        #region Commands

        protected CloseChangerSubviewCommand okCommand;
        protected CloseChangerSubviewCommand cancelCommand;

        public CloseChangerSubviewCommand OkCommand
        {
            get { return okCommand; }
            set { okCommand = value; }
        }

        public CloseChangerSubviewCommand CancelCommand
        {
            get { return cancelCommand; }
            set { cancelCommand = value; }
        }

        #endregion // end of Commands
    }
}