﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using SunburstWPF.ChartModel;
using SunburstWPF.Commands.FileMenu;
using SunburstWPF.Commands.General;
using SunburstWPF.Commands.PresentationChange;
using SunburstWPF.Drawing;
using SunburstWPF.Helpers;
using SunburstWPF.Languages;
using Xceed.Wpf.Toolkit;
using MessageBox = System.Windows.MessageBox;

namespace SunburstWPF.ViewModels
{
    public class MainViewModel : PropertyChangedNotifier
    {
        private ChartDesigner chartDesigner;
        private bool isChartLoaded;

        public MainViewModel(ScrollViewer chartViewer, ToolBarTray mainToolbarTray, MenuItem useAbbreviationsMenuItem)
        {
            Grid chartSpace = chartViewer.FindName(SettingsHelper.GetChartGridName()) as Grid;
            IsChartLoaded = false;
            PrepareOpen(chartSpace, mainToolbarTray, useAbbreviationsMenuItem);
            PrepareSaveAsImage(chartViewer);
            PrepareClose();
        }

        public bool IsChartLoaded
        {
            get { return isChartLoaded; }
            set
            {
                isChartLoaded = value;
                RaisePropertyChanged("IsChartLoaded");
            }
        }

        public event EventHandler RequestClose;

        public void OnMainWindowClosing(object sender, CancelEventArgs e)
        {
            Action afterCloseExecute = CloseCommand.AfterExecuted;
            CloseCommand.AfterExecuted = null;
            CloseCommand.Execute(new object());
            if (CloseCommand.Cancelled)
            {
                CloseCommand.AfterExecuted = afterCloseExecute;
                e.Cancel = true;
            }
        }

        #region Commands

        private OpenFileCommand openCommand = new OpenFileCommand();
        private SaveToFileCommand saveToFileCommand = new SaveToFileCommand();
        private SaveCommand saveCommand = new SaveCommand();
        private SaveAsImageCommand saveAsImageCommand;
        private CloseCommand closeCommand = new CloseCommand();
        private DecrementZoomCommand decrementZoomCommand = new DecrementZoomCommand();
        private IncrementZoomCommand incrementZoomCommand = new IncrementZoomCommand();
        private IncreaseAngleCommand increaseAngleCommand = new IncreaseAngleCommand();
        private DecreaseAngleCommand decreaseAngleCommand = new DecreaseAngleCommand();
        private ChangeColorCommand changeBackgroundCommand = new ChangeColorCommand(Lang.SetBackgroundColor);
        private ChangeLineStyleCommand changeBordersCommand = new ChangeLineStyleCommand(Lang.SetBorderProperties);
        private ChangeFontStyleCommand changeTitleStyleCommand = new ChangeFontStyleCommand(Lang.Title);
        private ChangeFontStyleCommand changeDescriptionStyleCommand = new ChangeFontStyleCommand(Lang.Description);
        private ChangeFontStyleCommand changeLabelStyleCommand = new ChangeFontStyleCommand(Lang.Labels);
        private ChangeLanguageCommand changeLanguageCommand = new ChangeLanguageCommand();
        private ShowAboutCommand aboutCommand = new ShowAboutCommand();
        private LevelUpCommand goLevelUpCommand = new LevelUpCommand();
        private SetAsRootCommand goToRootCommand = new SetAsRootCommand(Lang.GoToTheRoot, 0, true);

        #endregion // end of Commands

        #region CommandsProperties

        public OpenFileCommand OpenCommand
        {
            get { return openCommand; }
            set { openCommand = value; }
        }

        public SaveToFileCommand SaveAsCommand
        {
            get { return saveToFileCommand; }
            set { saveToFileCommand = value; }
        }

        public SaveCommand SaveCommand
        {
            get { return saveCommand; }
            set { saveCommand = value; }
        }

        public SaveAsImageCommand SaveAsImageCommand
        {
            get { return saveAsImageCommand; }
            set { saveAsImageCommand = value; }
        }

        public CloseCommand CloseCommand
        {
            get { return closeCommand; }
            set { closeCommand = value; }
        }

        public DecrementZoomCommand DecrementZoomCommand
        {
            get { return decrementZoomCommand; }
            set { decrementZoomCommand = value; }
        }

        public IncrementZoomCommand IncrementZoomCommand
        {
            get { return incrementZoomCommand; }
            set { incrementZoomCommand = value; }
        }

        public IncreaseAngleCommand IncreaseAngleCommand
        {
            get { return increaseAngleCommand; }
            set { increaseAngleCommand = value; }
        }

        public DecreaseAngleCommand DecreaseAngleCommand
        {
            get { return decreaseAngleCommand; }
            set { decreaseAngleCommand = value; }
        }

        public ChangeColorCommand ChangeBackgroundCommand
        {
            get { return changeBackgroundCommand; }
            set { changeBackgroundCommand = value; }
        }

        public ChangeLineStyleCommand ChangeBordersCommand
        {
            get { return changeBordersCommand; }
            set { changeBordersCommand = value; }
        }

        public ChangeFontStyleCommand ChangeTitleStyleCommand
        {
            get { return changeTitleStyleCommand; }
            set { changeTitleStyleCommand = value; }
        }

        public ChangeFontStyleCommand ChangeDescriptionStyleCommand
        {
            get { return changeDescriptionStyleCommand; }
            set { changeDescriptionStyleCommand = value; }
        }

        public ChangeFontStyleCommand ChangeLabelStyleCommand
        {
            get { return changeLabelStyleCommand; }
            set { changeLabelStyleCommand = value; }
        }

        public ChangeLanguageCommand ChangeLanguageCommand
        {
            get { return changeLanguageCommand; }
            set { changeLanguageCommand = value; }
        }

        public ShowAboutCommand AboutCommand
        {
            get { return aboutCommand; }
            set { aboutCommand = value; }
        }

        public LevelUpCommand GoLevelUpCommand
        {
            get { return goLevelUpCommand; }
            set { goLevelUpCommand = value; }
        }

        public SetAsRootCommand GoToRootCommand
        {
            get { return goToRootCommand; }
            set { goToRootCommand = value; }
        }

        #endregion // end of CommandsProperties

        #region Command Preparation

        private void PrepareOpen(Grid chartSpace, ToolBarTray mainToolbarTray, MenuItem useAbbreviationsMenuItem)
        {
            OpenCommand.AfterExecuted = preparedChart =>
            {
                if (preparedChart != null)
                {
                    SaveAsCommand.BeforeExecuted = () => { return preparedChart; };
                    SaveCommand.BeforeExecuted = SaveAsCommand.BeforeExecuted;
                    CloseCommand.BeforeExecute.Invoke();
                    CheckCohesion(preparedChart);
                    chartDesigner = new ChartDesigner(preparedChart, chartSpace);
                    PrepareIncrementDecrementZoom(preparedChart);
                    PrepareIncreaseDecreaseAngle(preparedChart);
                    PrepareToolbars(preparedChart, mainToolbarTray, useAbbreviationsMenuItem);
                    PrepareChartSpace(preparedChart, chartSpace);
                    PrepareLabelsTreeView(preparedChart, chartSpace);
                    PrepareChangeBackground(preparedChart.GeneralChartDesign.BackgroundColor);
                    PrepareChangeBorders(preparedChart.GeneralChartDesign.ChartBorders);
                    PrepareChangeFontStyle(ChangeTitleStyleCommand, preparedChart.Title.Font);
                    PrepareChangeFontStyle(ChangeDescriptionStyleCommand, preparedChart.Description.Font);
                    PrepareChangeFontStyle(ChangeLabelStyleCommand, preparedChart.GeneralChartDesign.LabelFont);
                    PrepareChangeLevel(preparedChart);
                    IsChartLoaded = true;
                }
            };
        }

        private void PrepareSaveAsImage(ScrollViewer chartViewer)
        {
            saveAsImageCommand = new SaveAsImageCommand(chartViewer);
        }

        private void PrepareClose()
        {
            CloseCommand.BeforeExecute = () =>
            {
                if (IsChartLoaded)
                {
                    MessageBoxResult result = MessageBox.Show(Lang.DoWantToSaveChanges, Lang.Close,
                        MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
                    if (result.Equals(MessageBoxResult.Yes))
                        SaveCommand.Execute(new object());
                    else if (result.Equals(MessageBoxResult.Cancel))
                        CloseCommand.Cancelled = true;
                    else CloseCommand.Cancelled = false;
                }
            };
            CloseCommand.AfterExecuted = () =>
            {
                if (RequestClose != null)
                    RequestClose.Invoke(this, EventArgs.Empty);
            };
        }

        private void PrepareIncrementDecrementZoom(Chart chart)
        {
            DecrementZoomCommand.BeforeExecuted = () => { return chart.GeneralChartDesign.Zoom; };
            IncrementZoomCommand.BeforeExecuted = DecrementZoomCommand.BeforeExecuted;
            DecrementZoomCommand.AfterExecuted = newZoom =>
            {
                // calling PossibleMinimumZoom prevents decreasing value too far
                chart.GeneralChartDesign.Zoom = chartDesigner.PossibleMinimumZoom(newZoom);
            };
            IncrementZoomCommand.AfterExecuted = DecrementZoomCommand.AfterExecuted;
        }

        private void PrepareIncreaseDecreaseAngle(Chart chart)
        {
            DecreaseAngleCommand.BeforeExecuted = () => { return chart.GeneralChartDesign.RotationClockwise; };
            IncreaseAngleCommand.BeforeExecuted = DecreaseAngleCommand.BeforeExecuted;
            DecreaseAngleCommand.AfterExecuted = newAngle => { chart.GeneralChartDesign.RotationClockwise = newAngle; };
            IncreaseAngleCommand.AfterExecuted = DecreaseAngleCommand.AfterExecuted;
        }

        private void PrepareToolbars(Chart chart, ToolBarTray mainToolbarTray, MenuItem useAbbreviationsMenuItem)
        {
            Slider zoomSlider = mainToolbarTray.FindName("ZoomSlider") as Slider;
            IntegerUpDown zoomUpDown = mainToolbarTray.FindName("ZoomUpDownBox") as IntegerUpDown;
            DoubleUpDown totalRadiusUpDown = mainToolbarTray.FindName("TotalRadiusUpDownBox") as DoubleUpDown;
            DoubleUpDown innerRadiusUpDown = mainToolbarTray.FindName("InnerRadiusUpDownBox") as DoubleUpDown;
            DoubleUpDown angleUpDown = mainToolbarTray.FindName("AngleUpDownBox") as DoubleUpDown;
            CheckBox usingAbbreviations = mainToolbarTray.FindName("AbbrevCheckbox") as CheckBox;

            if (zoomSlider != null)
                Binder.SetNewBinding("Zoom", chart.GeneralChartDesign, zoomSlider, Slider.ValueProperty);
            if (zoomUpDown != null)
                Binder.SetNewBinding("Zoom", chart.GeneralChartDesign, zoomUpDown, IntegerUpDown.ValueProperty);
            if (totalRadiusUpDown != null)
                Binder.SetNewBinding("TotalRadius", chart.GeneralChartDesign, totalRadiusUpDown,
                    DoubleUpDown.ValueProperty);
            if (innerRadiusUpDown != null)
                Binder.SetNewBinding("InnerRadius", chart.GeneralChartDesign, innerRadiusUpDown,
                    DoubleUpDown.ValueProperty);
            if (angleUpDown != null)
                Binder.SetNewBinding("RotationClockwise", chart.GeneralChartDesign, angleUpDown,
                    DoubleUpDown.ValueProperty);
            if (usingAbbreviations != null)
            {
                Binder.SetNewBinding("HasAllAbbreviations", chart, usingAbbreviations, CheckBox.IsEnabledProperty);
                Binder.SetNewBinding("UsingAbbreviations", chart.GeneralChartDesign, usingAbbreviations,
                    CheckBox.IsCheckedProperty);
            }
            Binder.SetNewBinding("HasAllAbbreviations", chart, useAbbreviationsMenuItem, MenuItem.IsEnabledProperty);
            Binder.SetNewBinding("UsingAbbreviations", chart.GeneralChartDesign, useAbbreviationsMenuItem,
                MenuItem.IsCheckedProperty);
        }

        private void PrepareLabelsTreeView(Chart chart, Grid chartSpace)
        {
            TreeView labelsView = chartSpace.FindName("ChartLabels") as TreeView;
            Binder.SetNewBinding("RootsToDraw", chartDesigner, labelsView, TreeView.ItemsSourceProperty);
            labelsView.ContextMenu = ContextMenuBuilder.FontChangeMenu(chart.GeneralChartDesign.LabelFont);
        }

        private void PrepareChartSpace(Chart chart, Grid chartSpace)
        {
            chartSpace.DataContext = chart;
        }

        private void PrepareChangeBackground(ARGBColor backgroundColor)
        {
            ChangeBackgroundCommand.ColorToChange = backgroundColor;
        }

        private void PrepareChangeBorders(LineStyleType borderStyle)
        {
            ChangeBordersCommand.LineStyleToChange = borderStyle;
        }

        private void PrepareChangeFontStyle(ChangeFontStyleCommand command, FontDefinitionType font)
        {
            command.FontStyleToChange = font;
        }

        private void PrepareChangeLevel(Chart chart)
        {
            GoLevelUpCommand.ChartToModify = chart;
            GoToRootCommand.GeneralDesign = chart.GeneralChartDesign;
        }

        #endregion // end of Command Preparation

        #region Coherence Checks

        // checks whether information about chart is coherent:
        // 1) info about root and its ancestors: whether root node 
        // should be displayed according to its ancestors being expanded or not
        // even if root "should" be invisible, it will be shown (visibility is not checked for the root, it's just always drawn), 
        // but user is informed about the issue and asked whether he/she wants it to be repaired (set all ancestors expanded)
        // 2) info about abbreviations: if there are some abbreviations missing, setting UsingAbbreviations (in GeneraChartDesign) to true
        // is pointless, because this option in that case is disabled anyway (this may be annoying, because the checkboxes for changing 
        // UsingAbbreviations will be checked AND disabled)
        private void CheckCohesion(Chart chart)
        {
            int rootId = chart.GeneralChartDesign.CurrentRootId;
            if (rootId == 0) // means: show the whole chart, so nothing to check
                return;
            NodeType rootNode = chart.GetNodeById(rootId);
            List<NodeType> incoherentList = FindIncoherentRootAncestors(rootNode);

            // if there are abbreviations missing, using abbreviations is blocked, so UsingAbbreviations = true is pointless
            bool abbreviationsWantedButImpossible = !chart.HasAllAbbreviations &&
                                                    chart.GeneralChartDesign.UsingAbbreviations;

            if (incoherentList.Count > 0 || abbreviationsWantedButImpossible)
            {
                string header = "Data in given file is incoherent. See messages below." + Environment.NewLine +
                                Environment.NewLine;
                string question = Environment.NewLine + "Do you want to repair the file?";

                string incoherentRootVisibility = string.Format("{0}{1}{2}{3}{4}", "• The root node must be ",
                                                      string.Format("{0} (id = {1})", rootNode.Name, rootNode.Id),
                                                      ", but values of 'Expanded' parameter in its ancestors: " +
                                                      Environment.NewLine,
                                                      NodesListText(incoherentList),
                                                      "suggest that this node should be invisible. The chart will be shown anyway, " +
                                                      "but the program may repair this inconsistency (set the 'Expanded' values of nodes " +
                                                      "listed above to true). ") + Environment.NewLine;

                string abbreviationsImpossible =
                    "• There are some missing abbreviations in the source file, which means " +
                    "that using them will be disabled. However, the 'UsingAbbreviations' attribute of general chart design is set to 'true'. " +
                    "This will not affect the chart presentation, but the program may repair this inconsistency (set the 'UsingAbbreviations' value " +
                    "to 'false'). " + Environment.NewLine;

                string explanationText = string.Format("{0}{1}{2}{3}", header,
                    incoherentList.Count > 0 ? incoherentRootVisibility : "",
                    abbreviationsWantedButImpossible ? abbreviationsImpossible : "",
                    question);

                MessageBoxResult result = MessageBox.Show(explanationText, Lang.IncoherentData,
                    MessageBoxButton.YesNo, MessageBoxImage.Question);

                if (result.Equals(MessageBoxResult.Yes))
                {
                    if (incoherentList.Count > 0)
                        ExpandAll(incoherentList);
                    if (abbreviationsWantedButImpossible)
                        chart.GeneralChartDesign.UsingAbbreviations = false;

                    SaveCommand.Execute(new object());
                }
            }
        }

        private List<NodeType> FindIncoherentRootAncestors(NodeType rootNode)
        {
            NodeType current = rootNode;
            List<NodeType> incoherentList = new List<NodeType>(rootNode.NestingLevel);

            while (current.Parent != null)
            {
                if (current.Parent.Expanded == false)
                    incoherentList.Add(current.Parent);
                current = current.Parent;
            }
            return incoherentList;
        }

        private void ExpandAll(List<NodeType> incoherentList)
        {
            foreach (NodeType incoherent in incoherentList)
                incoherent.Expanded = true;
        }

        private string NodesListText(List<NodeType> nodes)
        {
            StringBuilder sb = new StringBuilder();
            foreach (NodeType node in nodes)
                sb.AppendLine(string.Format("{0} (id = {1})", node.Name, node.Id));
            return sb.ToString();
        }

        #endregion // end of Coherence Checks
    }
}