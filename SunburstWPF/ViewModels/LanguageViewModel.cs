﻿using System.Collections.ObjectModel;
using System.Globalization;
using SunburstWPF.Helpers;

namespace SunburstWPF.ViewModels
{
    public class LanguageViewModel : ChangerViewModelBase
    {
        private CultureInfo selectedCulture;

        public LanguageViewModel()
        {
            SelectedCulture = SettingsHelper.GetCurrentLanguage();
            OkCommand.BeforeExecuted += () =>
            {
                SettingsHelper.SetCurrentLanguage(SelectedCulture);
                SettingsHelper.ActivateCurrentLanguage();
                SettingsHelper.SaveSettings();
            };
        }

        public CultureInfo SelectedCulture
        {
            get { return selectedCulture; }
            set
            {
                selectedCulture = value;
                RaisePropertyChanged("SelectedCulture");
            }
        }

        public ObservableCollection<CultureInfo> AvailableCultures
        {
            get { return SettingsHelper.GetSupportedLanguages(); }
        }

        protected override void CancelChanges()
        {
        }
    }
}