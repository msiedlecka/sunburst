﻿using System.Windows.Media;
using SunburstWPF.ChartModel;

namespace SunburstWPF.ViewModels
{
    public class ColorViewModel : ChangerViewModelBase
    {
        private ARGBColor changedColor;
        private Color originalColor;

        public ColorViewModel(ARGBColor colorToChange)
        {
            originalColor = colorToChange.Color;
            changedColor = colorToChange;
        }

        public Color ChangedColor
        {
            get { return changedColor.Color; }
            set
            {
                changedColor.Color = value;
                RaisePropertyChanged("ChangedColor");
            }
        }

        protected override void CancelChanges()
        {
            changedColor.Color = originalColor;
        }
    }
}