﻿using System.Windows.Media;
using SunburstWPF.ChartModel;

namespace SunburstWPF.ViewModels
{
    public class LinePropertyViewModel : ChangerViewModelBase
    {
        private LineStyleType lineStyle;
        private LineStyleType originalLineStyle;

        public LinePropertyViewModel(LineStyleType line)
        {
            lineStyle = line;
            originalLineStyle = new LineStyleType(line.Color.Color, line.WidthPx);
        }

        public Color ChangedColor
        {
            get { return lineStyle.Color.Color; }
            set
            {
                lineStyle.Color.Color = value;
                RaisePropertyChanged("ChangedColor");
            }
        }

        public double LineWidth
        {
            get { return lineStyle.WidthPx; }
            set
            {
                lineStyle.WidthPx = value;
                RaisePropertyChanged("LineWidth");
            }
        }

        protected override void CancelChanges()
        {
            lineStyle.Color.Color = originalLineStyle.Color.Color;
            lineStyle.WidthPx = originalLineStyle.WidthPx;
        }
    }
}