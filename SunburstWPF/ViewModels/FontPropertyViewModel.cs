﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using SunburstWPF.ChartModel;

namespace SunburstWPF.ViewModels
{
    public class FontPropertyViewModel : ChangerViewModelBase
    {
        private FontDefinitionType font;
        private FontDefinitionType originalFont;

        public FontPropertyViewModel(FontDefinitionType f)
        {
            font = f;
            originalFont = new FontDefinitionType(f.Family, f.Size, f.Color.Color, f.FontStyle, f.FontStretch,
                f.FontWeight);
        }

        public FontDefinitionType Font
        {
            get { return font; }
            set { font = value; }
        }

        public ICollection<FontFamily> AvailableFontFamilies
        {
            get
            {
                return
                    new ObservableCollection<FontFamily>(
                        Fonts.SystemFontFamilies.OrderBy(font => { return font.Source; },
                            StringComparer.CurrentCultureIgnoreCase));
            }
        }

        public ICollection<FontStyle> AvailableFontStyles
        {
            get
            {
                return new ObservableCollection<FontStyle>
                {
                    FontStyles.Normal,
                    FontStyles.Italic,
                    FontStyles.Oblique
                };
            }
        }

        protected override void CancelChanges()
        {
            font.Family = originalFont.Family;
            font.Size = originalFont.Size;
            font.Color.Color = originalFont.Color.Color;
            font.FontStyle = originalFont.FontStyle;
            font.FontStretch = originalFont.FontStretch;
            font.FontWeight = originalFont.FontWeight;
        }
    }
}