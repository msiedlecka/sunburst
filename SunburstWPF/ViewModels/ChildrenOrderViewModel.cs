﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using SunburstWPF.ChartModel;
using SunburstWPF.Commands.Subview;
using SunburstWPF.Languages;

namespace SunburstWPF.ViewModels
{
    public class ChildrenOrderViewModel : ChangerViewModelBase
    {
        private ObservableCollection<NodeType> changedChildren;
        private NodeType parent;
        private int selectedNodeIndex = -1;

        public ChildrenOrderViewModel(NodeType parentNode)
        {
            Parent = parentNode;
            ChangedChildren = new ObservableCollection<NodeType>(Parent.ChildNodes);

            MoveSelectedUpCommand = new MoveChildNodeCommand(Lang.MoveNodeUp,
                new KeyGesture(Key.U, ModifierKeys.Control), ChangedChildren, false);
            MoveSelectedDownCommand = new MoveChildNodeCommand(Lang.MoveNodeDown,
                new KeyGesture(Key.D, ModifierKeys.Control), ChangedChildren, true);

            OkCommand.BeforeExecuted +=
                () => {
                    Parent.ChildNodes = ChangedChildren.ToList(); // should raise "ChildNodes" property changed
                };
        }

        public MoveChildNodeCommand MoveSelectedUpCommand { get; set; }

        public MoveChildNodeCommand MoveSelectedDownCommand { get; set; }

        private NodeType Parent
        {
            get { return parent; }
            set { parent = value; }
        }

        public ObservableCollection<NodeType> ChangedChildren
        {
            get { return changedChildren; }
            set
            {
                changedChildren = value;
                RaisePropertyChanged("ChangedChildren");
            }
        }

        public int SelectedNodeIndex
        {
            get { return selectedNodeIndex; }
            set
            {
                selectedNodeIndex = value;
                UpdateCommands();
                RaisePropertyChanged("SelectedNodeIndex");
            }
        }

        public string HeaderContent
        {
            get { return string.Format("{0} {1}", Lang.DescendantsOfNode, Parent.FullLabel); }
        }

        private void UpdateCommands()
        {
            MoveSelectedUpCommand.MovedNodeIndex = SelectedNodeIndex;
            MoveSelectedDownCommand.MovedNodeIndex = SelectedNodeIndex;
        }

        protected override void CancelChanges()
        {
        }
    }
}