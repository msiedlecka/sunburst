﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SunburstWPF.ChartModel
{
    [Serializable]
    public class StyledText : PropertyChangedNotifier
    {
        public StyledText()
        {
        }

        public StyledText(string value, FontDefinitionType font)
        {
            Value = value;
            Font = font;
        }

        #region Fields

        private string value;

        private FontDefinitionType font;

        #endregion // end of Fields

        #region Properties

        [XmlElement("value", Form = XmlSchemaForm.Unqualified)]
        public string Value
        {
            get { return value; }
            set
            {
                this.value = value;
                RaisePropertyChanged("Value");
            }
        }

        [XmlElement("font", Form = XmlSchemaForm.Unqualified)]
        public FontDefinitionType Font
        {
            get { return font; }
            set
            {
                font = value;
                RaisePropertyChanged("Font");
            }
        }

        #endregion // end of Properties

        #region Methods

        // if any of the properties of the Font is missing, it is filled with 
        // the corresponding value
        public void FillMissingStyles(FontFamily family, double size, Color color, FontStyle style, FontStretch stretch,
            FontWeight weight)
        {
            FillMissingStyles(new FontDefinitionType(family, size, color, style, stretch, weight));
        }

        public void FillMissingStyles(FontDefinitionType font)
        {
            if (Font == null)
                Font = new FontDefinitionType();

            Font.FillMissingStyles(font);
        }

        #endregion // end of Methods
    }
}