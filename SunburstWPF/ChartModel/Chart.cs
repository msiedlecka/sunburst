﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Windows.Media;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SunburstWPF.ChartModel
{
    [Serializable]
    [XmlRoot("chart", Namespace = "", IsNullable = false)]
    public class Chart : PropertyChangedNotifier
    {
        #region Fields

        private StyledText title;

        private StyledText description;

        private string unit = "";

        private GeneralChartDesign generalChartDesign;

        private List<NodeType> data = new List<NodeType>();

        private bool hasAllAbbreviations;

        private int deepestLevel;

        #endregion Fields

        #region Properties

        [XmlElement("title", Form = XmlSchemaForm.Unqualified)]
        public StyledText Title
        {
            get { return title; }
            set
            {
                title = value;
                RaisePropertyChanged("Title");
            }
        }

        [XmlElement("description", Form = XmlSchemaForm.Unqualified)]
        public StyledText Description
        {
            get { return description; }
            set
            {
                description = value;
                RaisePropertyChanged("Description");
            }
        }

        [XmlElement("unit", Form = XmlSchemaForm.Unqualified)]
        public string Unit
        {
            get { return unit; }
            set
            {
                unit = value;
                RaisePropertyChanged("Unit");
            }
        }

        [XmlElement("general-chart-design", Form = XmlSchemaForm.Unqualified)]
        public GeneralChartDesign GeneralChartDesign
        {
            get { return generalChartDesign; }
            set
            {
                generalChartDesign = value;
                RaisePropertyChanged("GeneralChartDesign");
            }
        }

        [XmlArray("data", Form = XmlSchemaForm.Unqualified)]
        [XmlArrayItem("node", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
        public List<NodeType> Data
        {
            get { return data; }
            set
            {
                data = value;
                RaisePropertyChanged("Data");
            }
        }

        [XmlIgnore]
        public bool HasAllAbbreviations
        {
            get { return hasAllAbbreviations; }
            set
            {
                hasAllAbbreviations = value;
                RaisePropertyChanged("HasAllAbbreviations");
            }
        }

        [XmlIgnore]
        public int DeepestLevel
        {
            get { return deepestLevel; }
        }

        [XmlIgnore]
        public string PathToFile { get; set; }

        #endregion // end of Properties

        #region Methods

        // ShouldSerializeXXX() - see GeneralChartDesign.cs
        public bool ShouldSerializeUnit()
        {
            return Unit != null && Unit != "";
        }

        public void SetParents()
        {
            foreach (NodeType child in Data)
                child.SetDescendantsParents(null);
        }

        public void PropagateUnits()
        {
            foreach (NodeType child in Data)
                child.SetDescendantsUnit(Unit);
        }

        public NodeType GetNodeById(int id)
        {
            NodeType returned;
            foreach (NodeType node in Data)
            {
                returned = node.GetDescendantById(id);
                if (returned != null)
                    return returned;
            }
            return null;
        }

        public void FillMissingStyles(FontDefinitionType titleFont, FontDefinitionType descriptionFont,
            FontDefinitionType labelFont,
            Color defaultBackgroundColor, LineStyleType defaultLabelPointersStyle, LineStyleType defaultBordersStyle,
            StringCollection defaultPalette,
            int defaultZoom, double defaultRotation = 0.0)
        {
            int currentPaletteColor = 0;
            double nodeColorLightenPercentage = 0.0;

            Title.FillMissingStyles(titleFont);
            if (Description == null)
                Description = new StyledText();
            Description.FillMissingStyles(descriptionFont);

            GeneralChartDesign.FillMissingStyles(labelFont, defaultBackgroundColor, defaultLabelPointersStyle,
                defaultBordersStyle, defaultZoom, defaultRotation);

            if (defaultPalette.Count > 0)
                foreach (NodeType node in Data)
                    node.FillMissingColorRecursive(defaultPalette, ref currentPaletteColor,
                        ref nodeColorLightenPercentage);
        }

        public void CheckAllAbbreviations()
        {
            bool allAbbreviationsPresent = true;
            foreach (NodeType node in Data)
                if (!node.CheckAllAbbreviations())
                {
                    allAbbreviationsPresent = false;
                    break;
                }
            HasAllAbbreviations = allAbbreviationsPresent;
        }

        // deepestLevel = 1 means that there's only one node with no children
        public void FindDeepestLevel()
        {
            int childLevel = 0;
            int deepestLevel = 0;
            foreach (NodeType node in Data)
            {
                childLevel = node.DeepestLevel(1);
                if (childLevel > deepestLevel)
                    deepestLevel = childLevel;
            }
            this.deepestLevel = deepestLevel;
        }

        // returns 0 if everything's fine; 
        // otherwise, returns id of the first node where incorrect data was encountered
        public int IsDataCorrect()
        {
            int returned = 0;
            foreach (NodeType child in Data)
            {
                returned = child.IsDataCorrect();
                if (returned != 0)
                    return returned;
            }
            return 0;
        }

        public double TopChildrenSum()
        {
            double sum = 0.0;
            foreach (NodeType child in Data)
                sum += child.Number;
            return sum;
        }

        #endregion // end of Methods
    }
}