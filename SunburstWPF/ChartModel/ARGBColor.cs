﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Media;
using System.Xml.Serialization;

namespace SunburstWPF.ChartModel
{
    [Serializable]
    public class ARGBColor : PropertyChangedNotifier
    {
        private static string noColor = "#00000000";
        private Color color;
        private string htmlFormatColor;

        private void Initialize()
        {
            color = ColorFromHtmlFormat(htmlFormatColor);
        }

        // sets channels in Color fields based on value of HtmlFormatColor property
        public static Color ColorFromHtmlFormat(string htmlFormatColor)
        {
            Color c = new Color();
            string unhashedHtmlFormatColor = htmlFormatColor.Substring(1); // get rid of #
            c.A = GetA(unhashedHtmlFormatColor);
            c.R = GetR(unhashedHtmlFormatColor);
            c.G = GetG(unhashedHtmlFormatColor);
            c.B = GetB(unhashedHtmlFormatColor);
            return c;
        }

        private void RaiseAllChannelsChanged()
        {
            RaisePropertyChanged("Red");
            RaisePropertyChanged("Green");
            RaisePropertyChanged("Blue");
            RaisePropertyChanged("Alpha");
            RaisePropertyChanged("Color");
        }

        // quick, to do better (with conversion to HSL/HSV)
        public void Lighten(double percentage)
        {
            Red = LightenChannel(Red, percentage);
            Green = LightenChannel(Green, percentage);
            Blue = LightenChannel(Blue, percentage);
        }

        private byte LightenChannel(byte channelValue, double percentage)
        {
            return (byte) Math.Min(255, Math.Ceiling(channelValue + 255.0*percentage));
        }

        #region Properties

        [XmlText]
        public string HtmlFormatColor
        {
            get { return htmlFormatColor; }
            set
            {
                htmlFormatColor = value;
                Initialize();
                RaiseAllChannelsChanged();
            }
        }

        // with every change of value in Color structure the HtmlFormatColor property needs to be changed accordingly
        // because the serialization relies on HtmlFormatColor's value

        [XmlIgnore]
        public Color Color
        {
            get { return color; }
            set
            {
                color = value;
                HtmlFormatColor = ToHtmlStringFormat();
                RaiseAllChannelsChanged();
            }
        }

        [XmlIgnore]
        public byte Red
        {
            get { return color.R; }
            set
            {
                color.R = value;
                HtmlFormatColor = ToHtmlStringFormat();
                RaisePropertyChanged("Red");
                RaisePropertyChanged("Color");
            }
        }

        [XmlIgnore]
        public byte Green
        {
            get { return color.G; }
            set
            {
                color.G = value;
                RaisePropertyChanged("Green");
                RaisePropertyChanged("Color");
            }
        }

        [XmlIgnore]
        public byte Blue
        {
            get { return color.B; }
            set
            {
                color.B = value;
                RaisePropertyChanged("Blue");
                RaisePropertyChanged("Color");
            }
        }

        [XmlIgnore]
        public byte Alpha
        {
            get { return color.A; }
            set
            {
                color.A = value;
                RaisePropertyChanged("Alpha");
                RaisePropertyChanged("Color");
            }
        }

        #endregion //end of Properties

        // if an object was constructed manually in the program, there's no need
        // to call Initialize() after that

        #region Constructors

        public ARGBColor()
            : this(noColor)
        {
        }

        public ARGBColor(Color color)
            : this(color.A, color.R, color.G, color.B)
        {
        }

        public ARGBColor(byte alpha, byte red, byte green, byte blue)
        {
            Alpha = alpha;
            Red = red;
            Green = green;
            Blue = blue;
            htmlFormatColor = ToHtmlStringFormat();
        }

        public ARGBColor(byte red, byte green, byte blue)
            : this(255, red, green, blue)
        {
        }

        public ARGBColor(string htmlFormatColor)
        {
            if (CheckIfHtmlFormat(htmlFormatColor))
                HtmlFormatColor = htmlFormatColor;
            else
                HtmlFormatColor = noColor;
            Initialize();
        }

        #endregion // end of Constructors

        #region HtmlColorConverterHelpers

        private static byte GetA(string unhashedHtmlFormatColor)
        {
            return GetChannelAt(unhashedHtmlFormatColor, 0);
        }

        private static byte GetR(string unhashedHtmlFormatColor)
        {
            return GetChannelAt(unhashedHtmlFormatColor, 2);
        }

        private static byte GetG(string unhashedHtmlFormatColor)
        {
            return GetChannelAt(unhashedHtmlFormatColor, 4);
        }

        private static byte GetB(string unhashedHtmlFormatColor)
        {
            return GetChannelAt(unhashedHtmlFormatColor, 6);
        }

        private static byte GetChannelAt(string unhashedHtmlFormatColor, int startPosition)
        {
            return Convert.ToByte(GetIntFromHex(unhashedHtmlFormatColor.Substring(startPosition, 2)));
        }

        private static int GetIntFromHex(string hexNumber)
        {
            int value;
            if (int.TryParse(hexNumber, NumberStyles.HexNumber, CultureInfo.InvariantCulture, out value))
                return value;
            return 0;
        }

        private string ToHtmlStringFormat()
        {
            return string.Format("#{0:X2}{1:X2}{2:X2}{3:X2}", color.A, color.R, color.G, color.B);
        }

        private static bool CheckIfHtmlFormat(string text)
        {
            string htmlFormat = @"#[0-9A-F]{8}";
            Regex regex = new Regex(htmlFormat);
            return regex.IsMatch(text);
        }

        #endregion // end of HtmlColorConverterHelpers
    }
}