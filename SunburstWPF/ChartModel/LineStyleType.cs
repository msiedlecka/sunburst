﻿using System;
using System.Windows.Media;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SunburstWPF.ChartModel
{
    [Serializable]
    public class LineStyleType : PropertyChangedNotifier
    {
        public LineStyleType()
        {
        }

        public LineStyleType(Color color, double width)
        {
            Color = new ARGBColor(color);
            WidthPx = width;
        }

        #region Fields

        private ARGBColor color;

        private double widthPx;

        #endregion // end of Fields

        #region Properties

        [XmlElement("color", Form = XmlSchemaForm.Unqualified)]
        public ARGBColor Color
        {
            get { return color; }
            set
            {
                color = value;
                RaisePropertyChanged("Color");
            }
        }

        [XmlElement("width-px", Form = XmlSchemaForm.Unqualified)]
        public double WidthPx
        {
            get { return widthPx; }
            set
            {
                widthPx = value;
                RaisePropertyChanged("WidthPx");
            }
        }

        #endregion // end of Properties

        #region Methods

        public void FillMissingStyles(Color color, double width)
        {
            if (Color == null)
                Color = new ARGBColor(color);
            if (WidthPx == 0)
                WidthPx = width;
        }

        public void FillMissingStyles(LineStyleType lineStyle)
        {
            FillMissingStyles(lineStyle.Color.Color, lineStyle.WidthPx);
        }

        #endregion // end of Methods
    }
}