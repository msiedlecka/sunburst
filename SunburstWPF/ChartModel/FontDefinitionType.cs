﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SunburstWPF.ChartModel
{
    [Serializable]
    public class FontDefinitionType : PropertyChangedNotifier
    {
        public FontDefinitionType()
        {
        }

        public FontDefinitionType(FontDefinitionType f)
            : this(f.Family, f.Size, f.Color.Color, f.FontStyle, f.FontStretch, f.FontWeight)
        {
        }

        public FontDefinitionType(FontFamily family, double size, Color color, FontStyle style, FontStretch stretch,
            FontWeight weight)
        {
            Family = family;
            Size = size;
            Color = new ARGBColor(color);
            FontStyle = style;
            FontStretch = stretch;
            FontWeight = weight;
        }

        #region Fields

        private double size;

        private ARGBColor color;

        private FontFamily family;

        private FontStyle style;

        private FontStretch stretch;

        private FontWeight weight;

        #endregion // end of Fields

        #region Properties

        [XmlElement("name", Form = XmlSchemaForm.Unqualified)]
        public string Name
        {
            get { return Family != null ? Family.Source : ""; }
            set
            {
                Family = new FontFamily(value);
                RaisePropertyChanged("Name");
                RaisePropertyChanged("Family");
            }
        }

        [XmlIgnore]
        public FontFamily Family
        {
            get { return family; }
            set
            {
                family = value;
                RaisePropertyChanged("Name");
                RaisePropertyChanged("Family");
            }
        }

        [XmlElement("size", Form = XmlSchemaForm.Unqualified)]
        public double Size
        {
            get { return size; }
            set
            {
                size = value;
                RaisePropertyChanged("Size");
            }
        }

        [XmlElement("color", Form = XmlSchemaForm.Unqualified)]
        public ARGBColor Color
        {
            get { return color; }
            set
            {
                color = value;
                RaisePropertyChanged("Color");
            }
        }

        [XmlElement("style", Form = XmlSchemaForm.Unqualified)]
        public string Style
        {
            get { return style != null ? style.ToString() : ""; }
            set
            {
                style = FontStyleType.FontStyleByName(value);
                RaisePropertyChanged("Style");
                RaisePropertyChanged("FontStyle");
            }
        }

        [XmlIgnore]
        public FontStyle FontStyle
        {
            get { return style; }
            set
            {
                style = value;
                RaisePropertyChanged("Style");
                RaisePropertyChanged("FontStyle");
            }
        }

        [XmlElement("stretch", Form = XmlSchemaForm.Unqualified)]
        public int Stretch
        {
            get { return stretch.ToOpenTypeStretch(); }
            set
            {
                stretch = FontStretch.FromOpenTypeStretch(value);
                RaisePropertyChanged("Stretch");
                RaisePropertyChanged("FontStretch");
            }
        }

        [XmlIgnore]
        public FontStretch FontStretch
        {
            get { return stretch; }
            set
            {
                stretch = value;
                RaisePropertyChanged("Stretch");
                RaisePropertyChanged("FontStretch");
            }
        }

        [XmlElement("weight", Form = XmlSchemaForm.Unqualified)]
        public int Weight
        {
            get { return weight.ToOpenTypeWeight(); }
            set
            {
                weight = FontWeight.FromOpenTypeWeight(value);
                RaisePropertyChanged("Weight");
                RaisePropertyChanged("FontWeight");
            }
        }

        [XmlIgnore]
        public FontWeight FontWeight
        {
            get { return weight; }
            set
            {
                weight = value;
                RaisePropertyChanged("Weight");
                RaisePropertyChanged("FontWeight");
            }
        }

        #endregion // end of Properties

        #region Methods

        public void FillMissingStyles(FontFamily family, double size, Color color, FontStyle style, FontStretch stretch,
            FontWeight weight)
        {
            if (Family == null)
                Family = family;
            if (Size == 0)
                Size = size;
            if (Color == null)
                Color = new ARGBColor(color);
            if (FontStyle == null)
                FontStyle = style;
            if (FontStretch == null)
                FontStretch = stretch;
            if (FontWeight == null)
                FontWeight = weight;
        }

        public void FillMissingStyles(FontDefinitionType font)
        {
            FillMissingStyles(font.Family, font.Size, font.Color.Color, font.FontStyle, font.FontStretch,
                font.FontWeight);
        }

        #endregion // end of Methods
    }
}