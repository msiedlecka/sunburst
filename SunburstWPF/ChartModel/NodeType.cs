﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Windows.Media;
using System.Xml.Schema;
using System.Xml.Serialization;
using SunburstWPF.Helpers;

namespace SunburstWPF.ChartModel
{
    [Serializable]
    public class NodeType : PropertyChangedNotifier
    {
        #region Fields

        private string name;

        private string abbrev;

        private double number;

        private ARGBColor color;

        private bool expanded;

        private List<NodeType> childNodes;

        private int id;

        private int nestingLevel;

        private NodeType parent;

        private string unit = "";

        #endregion // end of Fields

        #region Properties

        [XmlElement("name", Form = XmlSchemaForm.Unqualified)]
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                RaisePropertyChanged("Name");
                RaisePropertyChanged("FullLabel");
            }
        }

        [XmlElement("abbrev", Form = XmlSchemaForm.Unqualified)]
        public string Abbrev
        {
            get { return abbrev; }
            set
            {
                abbrev = value;
                RaisePropertyChanged("Abbrev");
            }
        }

        [XmlElement("number", Form = XmlSchemaForm.Unqualified)]
        public double Number
        {
            get { return number; }
            set
            {
                number = value;
                RaisePropertyChanged("Number");
                RaisePropertyChanged("FullLabel");
                RaisePropertyChanged("FormattedNumber");
            }
        }

        [XmlElement("color", Form = XmlSchemaForm.Unqualified)]
        public ARGBColor Color
        {
            get { return color; }
            set
            {
                color = value;
                RaisePropertyChanged("Color");
            }
        }

        [XmlElement("expanded", Form = XmlSchemaForm.Unqualified)]
        public bool Expanded
        {
            get { return expanded; }
            set
            {
                expanded = value;
                RaisePropertyChanged("Expanded");
            }
        }

        [XmlElement("node", Form = XmlSchemaForm.Unqualified)]
        public List<NodeType> ChildNodes
        {
            get { return childNodes; }
            set
            {
                childNodes = value;
                RaisePropertyChanged("ChildNodes");
            }
        }

        [XmlAttribute("id", Form = XmlSchemaForm.Unqualified)]
        public int Id
        {
            get { return id; }
            set
            {
                id = value;
                RaisePropertyChanged("Id");
            }
        }

        [XmlIgnore]
        public int NestingLevel
        {
            get { return nestingLevel; }
        }

        [XmlIgnore]
        public NodeType Parent
        {
            get { return parent; }
            private set { parent = value; }
        }

        [XmlIgnore]
        public string FullLabel
        {
            get { return string.Format("{0} ({1})", Name, FormattedNumberWithUnit); }
        }

        [XmlIgnore]
        public string FormattedNumberWithUnit
        {
            get { return FormattedNumber + (unit != "" ? " " + unit : ""); }
        }

        [XmlIgnore]
        public string FormattedNumber
        {
            get { return Number.ToString("N", SettingsHelper.GetCurrentLanguage()); }
        }

        [XmlIgnore]
        public double PercentOfParent
        {
            get
            {
                if (Parent != null)
                    return Number/Parent.ChildrenNumbersSum();
                return 1.0;
            }
        }

        #endregion // end of Properties

        #region Methods

        // returns if filling was needed
        private bool FillMissingColor(Color color, double lightenPercentage)
        {
            if (Color == null)
            {
                ARGBColor filling = new ARGBColor(color);
                filling.Lighten(lightenPercentage);
                Color = filling;
                return true;
            }
            return false;
        }

        // increases value of lightenPercentage by SettingsHelper.GetLightenPercentageStep()
        // if the value of lightenPercentage becomes too big (meaning of "too big" depends on implementation of
        // ARGBColor.Lighten()), it is set to zero back again
        private static double ChangeLightenPercentage(double lightenPercentage)
        {
            lightenPercentage += SettingsHelper.GetLightenPercentageStep();
            if (lightenPercentage >= 1.0)
                lightenPercentage = 0.0;
            return lightenPercentage;
        }

        // returns the next paletteColorNumber to be used for coloring
        public void FillMissingColorRecursive(StringCollection palette, ref int paletteColorNumber,
            ref double lightenPercentage)
        {
            int paletteSize = palette.Count;

            if (FillMissingColor(ARGBColor.ColorFromHtmlFormat(palette[paletteColorNumber]), lightenPercentage))
            {
                paletteColorNumber = (paletteColorNumber + 1)%paletteSize;
                if (paletteColorNumber == 0) // colors of the whole palette have been used with given lightenPercentage
                    lightenPercentage = ChangeLightenPercentage(lightenPercentage);
            }

            foreach (NodeType node in ChildNodes)
                node.FillMissingColorRecursive(palette, ref paletteColorNumber, ref lightenPercentage);
        }

        // returns null if node with given id is not present in this subtree
        public NodeType GetDescendantById(int id)
        {
            NodeType returnedByChild;

            if (Id == id)
                return this;
            foreach (NodeType child in ChildNodes)
            {
                returnedByChild = child.GetDescendantById(id);
                if (returnedByChild != null)
                    return returnedByChild;
            }
            return null;
        }

        public void SetDescendantsParents(NodeType parent)
        {
            Parent = parent;
            foreach (NodeType child in ChildNodes)
                child.SetDescendantsParents(this);
        }

        public void SetDescendantsUnit(string unit)
        {
            this.unit = unit;
            foreach (NodeType child in ChildNodes)
                child.SetDescendantsUnit(unit);
        }

        // returns false if node or any of its children do not have abbreviation set; true, when all have it set
        public bool CheckAllAbbreviations()
        {
            if (Abbrev == null || Abbrev.Equals(""))
                return false;
            foreach (NodeType node in ChildNodes)
                if (!node.CheckAllAbbreviations())
                    return false;
            return true;
        }

        // returns the greatest nesting level from all of node's descendants
        public int DeepestLevel(int currentLevel)
        {
            nestingLevel = currentLevel;

            int childLevel = 0;
            int maxLevel = currentLevel;
            foreach (NodeType node in ChildNodes)
            {
                childLevel = node.DeepestLevel(currentLevel + 1);
                if (childLevel > maxLevel)
                    maxLevel = childLevel;
            }
            return maxLevel;
        }

        // returns 0 if everything's fine; 
        // otherwise, returns id of the first node where incorrect data was encountered
        public int IsDataCorrect()
        {
            int returned = 0;
            foreach (NodeType child in ChildNodes)
            {
                returned = child.IsDataCorrect();
                if (returned != 0)
                    return returned;
            }
            if (ChildNodes.Count > 0)
                if (ChildrenNumbersSum() <= Number)
                    return 0;
                else return Id;
            return 0;
        }

        public double ChildrenNumbersSum()
        {
            double sum = 0.0;
            foreach (NodeType child in ChildNodes)
                sum += child.Number;
            return sum;
        }

        #endregion // end of Methods
    }
}