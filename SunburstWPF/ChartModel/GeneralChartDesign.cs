﻿using System;
using System.Windows.Media;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SunburstWPF.ChartModel
{
    [Serializable]
    [XmlType(TypeName = "general-chart-design")]
    public class GeneralChartDesign : PropertyChangedNotifier
    {
        #region Fields

        private FontDefinitionType labelFont;

        private LineStyleType labelPointersStyle;

        private int zoom;

        private double rotationClockwise;

        private ARGBColor backgroundColor;

        private LineStyleType chartBordersStyle;

        private int currentRootId;

        private double totalRadius;

        private double innerRadius;

        private bool usingAbbreviations;

        #endregion Fields

        #region Properties

        [XmlElement("label-font", Form = XmlSchemaForm.Unqualified)]
        public FontDefinitionType LabelFont
        {
            get { return labelFont; }
            set
            {
                labelFont = value;
                RaisePropertyChanged("LabelFont");
            }
        }

        [XmlElement("label-pointers", Form = XmlSchemaForm.Unqualified)]
        public LineStyleType LabelPointers
        {
            get { return labelPointersStyle; }
            set
            {
                labelPointersStyle = value;
                RaisePropertyChanged("LabelPointers");
            }
        }

        [XmlElement("zoom", Form = XmlSchemaForm.Unqualified)]
        public int Zoom
        {
            get { return zoom; }
            set
            {
                zoom = value;
                RaisePropertyChanged("Zoom");
                RaisePropertyChanged("ZoomedTotalDiameter");
                RaisePropertyChanged("ZoomedTotalRadius");
                RaisePropertyChanged("ZoomedInnerRadius");
            }
        }

        [XmlElement("rotation-clockwise", Form = XmlSchemaForm.Unqualified)]
        public double RotationClockwise
        {
            get { return rotationClockwise; }
            set
            {
                rotationClockwise = value;
                RaisePropertyChanged("RotationClockwise");
            }
        }

        [XmlElement("background-color", Form = XmlSchemaForm.Unqualified)]
        public ARGBColor BackgroundColor
        {
            get { return backgroundColor; }
            set
            {
                backgroundColor = value;
                RaisePropertyChanged("BackgroundColor");
            }
        }

        [XmlElement("chart-borders", Form = XmlSchemaForm.Unqualified)]
        public LineStyleType ChartBorders
        {
            get { return chartBordersStyle; }
            set
            {
                chartBordersStyle = value;
                RaisePropertyChanged("ChartBorders");
            }
        }

        [XmlElement("current-root-id", Form = XmlSchemaForm.Unqualified)]
        public int CurrentRootId
        {
            get { return currentRootId; }
            set
            {
                currentRootId = value;
                RaisePropertyChanged("CurrentRootId");
            }
        }

        [XmlElement("total-radius-lenght-px", Form = XmlSchemaForm.Unqualified)]
        public double TotalRadius
        {
            get { return totalRadius; }
            set
            {
                double minimum = InnerRadius;
                if (ChartBorders != null)
                    minimum += ChartBorders.WidthPx;
                if (value > minimum)
                {
                    totalRadius = value;
                    RaisePropertyChanged("TotalRadius");
                    RaisePropertyChanged("ZoomedTotalRadius");
                    RaisePropertyChanged("ZoomedTotalDiameter");
                }
            }
        }

        [XmlElement("inner-radius-lenght-px", Form = XmlSchemaForm.Unqualified)]
        public double InnerRadius
        {
            get { return innerRadius; }
            set
            {
                double maximum = TotalRadius;
                if (ChartBorders != null)
                    maximum += ChartBorders.WidthPx;
                if (value < maximum)
                {
                    innerRadius = value;
                    RaisePropertyChanged("InnerRadius");
                    RaisePropertyChanged("ZoomedInnerRadius");
                }
            }
        }

        [XmlElement("using-abbreviations", Form = XmlSchemaForm.Unqualified)]
        public bool UsingAbbreviations
        {
            get { return usingAbbreviations; }
            set
            {
                usingAbbreviations = value;
                RaisePropertyChanged("UsingAbbreviations");
            }
        }

        [XmlIgnore]
        public double ZoomedInnerRadius
        {
            get { return ZoomScale(InnerRadius, Zoom); }
        }

        [XmlIgnore]
        public double ZoomedTotalRadius
        {
            get { return ZoomScale(TotalRadius, Zoom); }
        }

        [XmlIgnore]
        public double ZoomedTotalDiameter
        {
            get { return ZoomedTotalRadius*2.0; }
        }

        #endregion Properties

        #region Methods

        // ShouldSerializeXXX() - tricks allowing NOT to serialize property XXX when it is not desirable (here - values not specified or equal to zero)
        // e.g. when zero is not allowed value for corresponding tags in xml, but may be present as values of these properties for some reason

        // CurrentRootId == 0 in program indicates that the whole tree is presented 
        // when it contains multiple nodes at top level (then it's a forest actually ;)
        public bool ShouldSerializeCurrentRootId()
        {
            return CurrentRootId != 0;
        }

        // values of TotalRadius and InnerRadius may be zero just after loading the source file data (without further 
        // preparations to draw the chart, which happen in ChartDesigner), when corresponding tags were not present
        public bool ShouldSerializeTotalRadius()
        {
            return TotalRadius > 0;
        }

        public bool ShouldSerializeInnerRadius()
        {
            return InnerRadius > 0;
        }

        public void FillMissingStyles(FontDefinitionType labelFont,
            Color defaultBackgroundColor, LineStyleType defaultLabelPointersStyle, LineStyleType defaultBorderStyle,
            int defaultZoom, double defaultRotation = 0.0)
        {
            if (LabelFont == null)
                LabelFont = new FontDefinitionType();

            LabelFont.FillMissingStyles(labelFont);

            if (LabelPointers == null)
                LabelPointers = new LineStyleType();

            LabelPointers.FillMissingStyles(defaultLabelPointersStyle);

            if (ChartBorders == null)
                ChartBorders = new LineStyleType();

            ChartBorders.FillMissingStyles(defaultBorderStyle);

            if (Zoom == 0)
                Zoom = defaultZoom;

            if (BackgroundColor == null)
                BackgroundColor = new ARGBColor(defaultBackgroundColor);
        }

        public static double ZoomScale(double value, int zoom)
        {
            return value*(zoom/100.0);
        }

        #endregion // end of Methods
    }
}