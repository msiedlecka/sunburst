﻿using System.Windows;

namespace SunburstWPF.ChartModel
{
    public class FontStyleType
    {
        public static FontStyle FontStyleByName(string name)
        {
            switch (name)
            {
                case "Normal":
                    return FontStyles.Normal;
                case "Italic":
                    return FontStyles.Italic;
                case "Oblique":
                    return FontStyles.Oblique;
                default:
                    return FontStyles.Normal;
            }
        }
    }
}