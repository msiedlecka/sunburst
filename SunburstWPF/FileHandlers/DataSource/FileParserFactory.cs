﻿using System;
using SunburstWPF.Helpers;

namespace SunburstWPF.FileHandlers.DataSource
{
    public abstract class FileParserFactory
    {
        public static IChartFileParser CreateParser(string fileExtension)
        {
            if (ExtensionsHelper.IsExtensionSupported(fileExtension, SettingsHelper.GetSupportedExtensions()))
                switch (fileExtension)
                {
                    case ".xml":
                        return new XmlParser();
                    default:
                        throw new NotImplementedException(ExtensionsHelper.notImplementedExceptionDescription);
                }
            throw new ArgumentException(ExtensionsHelper.argumentExceptionDescription, "fileExtension");
        }
    }
}