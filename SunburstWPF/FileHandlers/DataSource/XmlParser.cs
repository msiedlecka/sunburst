﻿using System;
using System.IO;
using System.Text;
using System.Windows;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using SunburstWPF.ChartModel;
using SunburstWPF.Languages;

namespace SunburstWPF.FileHandlers.DataSource
{
    public class XmlParser : IChartFileParser
    {
        private StringBuilder errors = new StringBuilder();
        private XmlReaderSettings settings;
        private bool validationSuccess = true;

        public Chart Parse(string sourcePath)
        {
            Chart chart;
            int incorrectNodeId = 0;
            Validate(sourcePath);
            if (validationSuccess)
            {
                XmlSerializer deserializer = new XmlSerializer(typeof(Chart));

                using (XmlReader reader = XmlReader.Create(sourcePath, settings))
                {
                    chart = (Chart) deserializer.Deserialize(reader);
                }

                errors.Clear(); // clear from potential validation warnings
                incorrectNodeId = chart.IsDataCorrect();
                if (incorrectNodeId != 0)
                {
                    errors.AppendFormat(
                        "Data error (parent node id = {0}): Sum of subelements is not equal to the sum of the parent element.",
                        incorrectNodeId);
                    InformAboutErrors(Lang.IncorrectData);
                    return null;
                }
                return chart;
            }
            InformAboutErrors(Lang.ValidationError);
            return null;
        }

        public void Save(Chart chart, string destinationPath)
        {
            using (FileStream fs = File.Create(destinationPath))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Chart));
                serializer.Serialize(fs, chart);
            }
        }

        private void ValidationCallback(object sender, ValidationEventArgs args)
        {
            if (args.Severity == XmlSeverityType.Warning)
                errors.AppendFormat("Warning: Matching schema not found. No validation occurred. Message: {0}",
                    args.Message + Environment.NewLine);
            else
            {
                validationSuccess = false;
                errors.AppendFormat("Validation error (line {0}, position {1}): {2}", args.Exception.LineNumber,
                    args.Exception.LinePosition, args.Message + Environment.NewLine);
            }
        }

        private void Validate(string sourcePath)
        {
            SetValidationSettings();
            ValidateXml(sourcePath, settings);
        }

        private void SetValidationSettings()
        {
            settings = new XmlReaderSettings();
            settings.Schemas.Add(null, "Resources/SunburstSchema.xsd");
            settings.ValidationType = ValidationType.Schema;
            settings.ValidationEventHandler += ValidationCallback;
        }

        private void ValidateXml(string sourcePath, XmlReaderSettings settings)
        {
            using (XmlReader reader = XmlReader.Create(sourcePath, settings))
            {
                while (reader.Read()) ;
            }
        }

        private void InformAboutErrors(string header)
        {
            MessageBox.Show(Lang.GivenFileIsNotCorrect + Environment.NewLine + errors, header,
                MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }
}