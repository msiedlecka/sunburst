﻿using SunburstWPF.ChartModel;

namespace SunburstWPF.FileHandlers.DataSource
{
    public interface IChartFileParser
    {
        Chart Parse(string sourcePath);
        void Save(Chart chart, string destinationPath);
    }
}