﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using SunburstWPF.Languages;

namespace SunburstWPF.FileHandlers.Image
{
    public abstract class ImageSaver
    {
        public void Save(FrameworkElement visual, string filePath)
        {
            SaveImage(visual, filePath, CreateEncoder());
        }

        protected void SaveImage(FrameworkElement visual, string filePath, BitmapEncoder encoder)
        {
            MeasureArrangeVisual(visual);
            try
            {
                RenderTargetBitmap bitmap = new RenderTargetBitmap((int) visual.ActualWidth, (int) visual.ActualHeight,
                    96, 96, PixelFormats.Pbgra32);
                bitmap.Render(visual);
                BitmapFrame frame = BitmapFrame.Create(bitmap);
                encoder.Frames.Add(frame);

                using (var stream = File.Create(filePath))
                {
                    encoder.Save(stream);
                }
            }
            catch (OutOfMemoryException ex)
            {
                MessageBox.Show(Lang.ErrorSavingHugeImage, Lang.Error, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        protected abstract BitmapEncoder CreateEncoder();

        public static void MeasureArrangeVisual(FrameworkElement visual)
        {
            double width = visual.ActualWidth;
            double height = visual.ActualHeight;
            Size size = new Size(width, height);

            visual.Measure(size);
            visual.Arrange(new Rect(size));
        }
    }
}