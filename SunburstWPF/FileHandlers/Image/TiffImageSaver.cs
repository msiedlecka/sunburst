﻿using System.Windows.Media.Imaging;

namespace SunburstWPF.FileHandlers.Image
{
    public class TiffImageSaver : ImageSaver
    {
        protected override BitmapEncoder CreateEncoder()
        {
            return new TiffBitmapEncoder();
        }
    }
}