﻿using System;
using SunburstWPF.Helpers;

namespace SunburstWPF.FileHandlers.Image
{
    public class ImageSaverFactory
    {
        public static ImageSaver CreateSaver(string fileExtension)
        {
            if (ExtensionsHelper.IsExtensionSupported(fileExtension, SettingsHelper.GetSupportedImageExtensions()))
                switch (fileExtension)
                {
                    case ".bmp":
                        return new BmpImageSaver();
                    case ".jpg":
                    case ".jpeg":
                        return new JpgImageSaver();
                    case ".png":
                        return new PngImageSaver();
                    case ".gif":
                        return new GifImageSaver();
                    case ".tiff":
                        return new TiffImageSaver();
                    default:
                        throw new NotImplementedException(ExtensionsHelper.notImplementedExceptionDescription);
                }
            throw new ArgumentException(ExtensionsHelper.argumentExceptionDescription, "fileExtension");
        }
    }
}