﻿using System.Windows.Media.Imaging;

namespace SunburstWPF.FileHandlers.Image
{
    public class JpgImageSaver : ImageSaver
    {
        protected override BitmapEncoder CreateEncoder()
        {
            return new JpegBitmapEncoder();
        }
    }
}