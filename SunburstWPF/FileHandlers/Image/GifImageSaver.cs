﻿using System.Windows.Media.Imaging;

namespace SunburstWPF.FileHandlers.Image
{
    public class GifImageSaver : ImageSaver
    {
        protected override BitmapEncoder CreateEncoder()
        {
            return new GifBitmapEncoder();
        }
    }
}